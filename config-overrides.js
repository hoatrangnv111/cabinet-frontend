const {
  override,
  fixBabelImports,
  addLessLoader,
  adjustWorkbox,
  overrideDevServer,
  watchAll
} = require('customize-cra');
const path = require('path');

module.exports = {
  webpack: override(

    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),
    addLessLoader({
      lessOptions: {
        javascriptEnabled: true,
        //modifyVars: { '@primary-color': '#1DA57A' },
      },
    }),
  ),
  devServer: overrideDevServer(
    watchAll()
  ),
  paths: function(paths, env) {
    paths.publicUrlOrPath = process.env.REACT_APP_PUBLISH_PATH || '/';
    return paths;
  }
};