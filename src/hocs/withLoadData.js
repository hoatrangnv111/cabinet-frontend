import React, { useCallback, useEffect, useState } from 'react'
// lib
import { useDispatch, useSelector } from 'react-redux'
// action
import dataActions from '../actions/dataActions'
// hook
import { useDebouncedCallback } from '../hooks/useDebounce'

import {queryStringExtensions} from '../extensions'

export const withLoadData =
  ({
     loadData,
     pathName
   }) => Component => {

    return props => {
      // state
      const [firstLoad, setFirstLoad] = useState(true)

      const state = useSelector(state => ({
        isLoading: state.fetching ? state.fetching.isLoading : false,
        data: state.data ? state.data.get(pathName) : null,
        user: state.auth ? state.auth.user || null : null
      }))

      // //reselect user
      // const user = useSelector(selectUserLogin())

      // dispatch
      const dispatch = useDispatch()
      /**
       * load list data
       * @type {function(*=)}
       */
      const loadDataPagerCallback = useCallback(
        (queryClause) => dispatch(
          dataActions.loadDataPager(queryClause, pathName)),
        [dispatch]
      )

      /**
       * load single data
       * @type {function(*=)}
       */
      const loadDataCallback = useCallback(
        (queryClause) => dispatch(dataActions.loadData(queryClause, pathName)),
        [dispatch]
      )

      // callback
      const [debouncedCallback] = useDebouncedCallback(
        () => loadData(initProps),
        100
      )

      const debounceLoadData = () => {
        debouncedCallback()
      }

      /**
       * load data when render page
       */
      useEffect(
        () => {
          debounceLoadData()
          if (firstLoad) {
            setFirstLoad(false)
          }
          return () => {
            setFirstLoad(true)
          }
        },
        []
      )


      const initProps = {
        ...props,
        data: state.data,
        objectId: queryStringExtensions.getIdParams(props),
        currentUser: state.user,
        isLoading: state.isLoading,
        isLoadingPage: state.data ? state.data.isLoading : false,
        loadDataPagerCallback,
        loadDataCallback
      }

      return <Component {...initProps} />
    }
  }