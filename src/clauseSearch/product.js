import { queryStringExtensions, stringExtensions } from '../extensions'
export default {
  initQuerySearchProduct(values, defaultPageSize) {
    const { pageSize, skip } = queryStringExtensions.getSizeAndIndexPage(
      values,
      defaultPageSize
    )

    let query = ``
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, name: {like: "%${keyword}%"}`
    }

    if (values.category) {
      const categoryId = stringExtensions.removeAccentedCharacter(
        values.category
      )
      query += `, categoryId: "${categoryId}"`
    }

    let order = `order`
    return {
      productClause: `where: {${query}}, limit: ${pageSize}, offset: ${skip}, order: "${order}"`,
      propertiesClause: `where: {isDeleted: false , isPublished: true}, order: "${order}"`
    }
  },

  initQueryUniqueProduct(productId) {
    const id = stringExtensions.removeEscapeCharacter(productId)
    return `id: "${id}"`
  }
}
