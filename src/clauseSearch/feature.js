import { queryStringExtensions, stringExtensions } from '../extensions'
export default {
  initQueryFeatures(values = {}) {
    let query = `isDeleted: false, isPublished: true `
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, name: {like: "%${keyword}%"}`
    }
    if (values.type) {
      query += `, type: ${values.type}`
    }
    let order = `order`
    return {
      whereClause: `where: {${query}}, order: "${order}"`
    }
  }
}
