import { queryStringExtensions, stringExtensions } from '../extensions'
export default {
  initQueryRootCategories(values = {}) {
    let query = `parentId: null, isDeleted: false, isPublished: true `
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, name: {like: "%${keyword}%"}`
    }

    // if (values.parent) {
    //   const parentId = stringExtensions.removeAccentedCharacter(values.parent)
    //   query += `, parentId: "${parentId}"`
    // }

    let order = `order`
    return {
      whereClause: `where: {${query}}, order: "${order}"`
    }
  },
  initQuerySearchCategory(values = {}, defaultPageSize) {
    const { pageSize, skip } = queryStringExtensions.getSizeAndIndexPage(
      values,
      defaultPageSize
    )

    let query = ``
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, name: {like: "%${keyword}%"}`
    }

    if (values.parent) {
      const parentId = stringExtensions.removeAccentedCharacter(values.parent)
      query += `, parentId: "${parentId}"`
    }

    let order = `order`
    return {
      whereClause: `where: {${query}}, limit: ${pageSize}, offset: ${skip}, order: "${order}"`,
      whereConnectionClause: `where: {${query}}, first: 1`
    }
  },

  initQueryUniqueCategory(categoryId) {
    const id = stringExtensions.removeEscapeCharacter(categoryId)
    return `id: "${id}"`
  },

  initQueryCreateOrUpdateCategory({ values, categoryId, isEditRow }) {
    let query = ``
    if (values.name) {
      const name = stringExtensions.removeEscapeCharacter(values.name)
      query += `, name: "${name}"`
    }

    if (!isEditRow) {
      const id = values?.parent?.id ?? values?.parent
      if (id) {
        const parentId = stringExtensions.removeEscapeCharacter(id)
        query += `, parentId: "${parentId}"`
      } else {
        query += `, parentId: null`
      }
    }

    if (typeof values.isPublished === 'boolean') {
      query += `, isPublished: ${!!values.isPublished}`
    } else {
      query += `, isPublished: true`
    }

    if (values.order) {
      query += `, order: ${values.order}`
    } else {
      query += `, order: 0`
    }

    return categoryId
      ? `data: {${query}}, where: {${this.initQueryUniqueCategory(categoryId)}}`
      : `data: {${query}}`
  },

  initQueryDeleteCategory(values, isDeleted) {
    const query = `isDeleted: ${!!isDeleted}`

    return `data: {${query}}, where: {id: "${values.id}"}`
  },

  initQuerySelectCategory(excludeId, parentLess) {
    let query = `isDeleted: false`

    if (excludeId) {
      query += `, id: {not: "${excludeId}"}`
    }

    if (parentLess) {
      query += `, parentId: null`
    }
    return `where: {${query}}`
  }
}
