import { queryStringExtensions, stringExtensions } from '../extensions'
export default {
  initQuerySearchProperty(values, defaultPageSize) {
    const { pageSize, skip } = queryStringExtensions.getSizeAndIndexPage(
      values,
      defaultPageSize
    )

    let query = ``
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, key: {like: "%${keyword}%"}`
    }

    let order = `order`
    return {
      whereClause: `where: {${query}}, limit: ${pageSize}, offset: ${skip}, order: "${order}"`,
      whereConnectionClause: `where: {${query}}, first: 1`
    }
  },

  initQueryUniqueProperty(propertyId) {
    const id = stringExtensions.removeEscapeCharacter(propertyId)
    return `id: "${id}"`
  },

  initQueryCreateOrUpdateProperty({ values, propertyIdId, isEditRow }) {
    try {
      let query = ``
      if (values.name) {
        const name = stringExtensions.removeEscapeCharacter(values.name)
        query += `, key: "${name}"`
      }

      if (!isEditRow) {
        const id = values?.product?.id ?? values?.product
        if (id) {
          const parentId = stringExtensions.removeEscapeCharacter(id)
          query += `, productId: "${parentId}"`
        } else {
          query += `, productId: null`
        }

        if (values.defaultValue) {
          const defaultValue = stringExtensions.removeEscapeCharacter(
            values.defaultValue
          )
          query += `, defaultValue: "${defaultValue}"`
        } else {
          query += `, defaultValue: null`
        }

        if (values.shortDescription) {
          const shortDescription = stringExtensions.removeEscapeCharacter(
            values.shortDescription
          )
          query += `, shortDescription: "${shortDescription}"`
        } else {
          query += `, shortDescription: null`
        }

        if (values.description) {
          const description = stringExtensions.removeEscapeCharacter(
            values.description
          )
          query += `, description: "${description}"`
        } else {
          query += `, description: null`
        }

        query += `, isShowDefault: ${!!values?.isShowDefault}`
      }

      query += `, isPublished: ${!values?.isPublished}`

      if (values.order) {
        query += `, order: ${values.order}`
      } else {
        query += `, order: 0`
      }

      return propertyIdId
        ? `data: {${query}}, where: {${this.initQueryUniqueProperty(
            propertyIdId
          )}}`
        : `data: {${query}}`
    } catch (error) {
      console.log('error.....', error)
    }
  },

  initQueryDeleteProperty(values, isDeleted) {
    const query = `isDeleted: ${!!isDeleted}`

    return `data: {${query}}, where: {id: "${values.id}"}`
  },

  initQuerySelectProperty(excludeId) {
    let query = `isDeleted: false`

    if (excludeId) {
      query += `, id: {not: "${excludeId}"}`
    }
    return `where: {${query}}`
  }
}
