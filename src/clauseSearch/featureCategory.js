import { queryStringExtensions, stringExtensions } from '../extensions'
import featureClause from './feature'
export default {
  initQueryFeatureCategories(values = {}) {
    let query = `isDeleted: false, isPublished: true `
    if (values.keyword) {
      const keyword = stringExtensions.removeEscapeCharacter(values.keyword)
      query += `, name: {like: "%${keyword}%"}`
    }
    let order = `createdAt`
    const clauseFeature = featureClause.initQueryFeatures()
    return {
      whereClause: `where: {${query}}, order: "${order}"`,
      whereClauseFeature: clauseFeature.whereClause
    }
  }
}
