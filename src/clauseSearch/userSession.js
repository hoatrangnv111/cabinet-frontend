import { queryStringExtensions, stringExtensions } from '../extensions'
export default {
  initQueryUserSessions(values = {}) {
    let query = `isDeleted: false, isPublished: true `
    if (values.user) {
      const userId = stringExtensions.removeEscapeCharacter(values.user)
      query += `, userId: "${userId}"`
    }
    let order = `order`
    return {
      whereClause: `where: {${query}}, order: "${order}"`
    }
  },
  initQueryUniqueUserSession(id) {
    return `id: "${stringExtensions.removeEscapeCharacter(id)}"`
  },
  initQueryCreateOrUpdateUserSession(values, user){
    let query = `isDeleted: false, isPublished: true `
    if (user) {
      const userId = stringExtensions.removeEscapeCharacter(user.id)
      query += `, userId: "${userId}"`
    }

    if(values.activeStyle){
      query += `, activeStyle: "${stringExtensions.removeEscapeCharacter(values.activeStyle)}"`
    }

    if (typeof values.isCollageStyle === 'boolean') {
      query += `, isCollageStyle: ${!!values.isCollageStyle}`
    } else {
      query += `, isCollageStyle: true`
    }

    if (values.userInfor) {
      const {firstName, lastName, email, phoneNumber, address, code} = values.userInfor
      if(firstName){
        query += `, firstName: "${stringExtensions.removeEscapeCharacter(firstName)}"`
      }

      if(lastName){
        query += `, lastName: "${stringExtensions.removeEscapeCharacter(lastName)}"`
      }

      if(email){
        query += `, email: "${stringExtensions.removeEscapeCharacter(email)}"`
      }

      if(phoneNumber){
        query += `, phoneNumber: "${stringExtensions.removeEscapeCharacter(phoneNumber)}"`
      }

      if(address){
        query += `, address: "${stringExtensions.removeEscapeCharacter(address)}"`
      }

      if(code){
        query += `, code: "${stringExtensions.removeEscapeCharacter(code)}"`
      }
    }

    if(values.userSessionStyles && values.userSessionStyles.length > 0){
      query +=`, userSessionStyles: [`
      values.userSessionStyles.map((item, key) => {
        query += `{
          featureCategoryId: "${stringExtensions.removeEscapeCharacter(item.featureCategoryId)}", 
          featureId: "${stringExtensions.removeEscapeCharacter(item.featureId)}", 
          featureProductId: "${stringExtensions.removeEscapeCharacter(item.featureProductId)}", 
          finishListId: "${stringExtensions.removeEscapeCharacter(item.finishListId)}"
        },`
      })
      query +=`]`
    }

    return values.id
      ? `data: {${query}}, where: {${this.initQueryUniqueUserSession(values.id)}}`
      : `data: {${query}}`
  }
}
