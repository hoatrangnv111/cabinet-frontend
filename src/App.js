import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { PersistGate } from 'redux-persist/es/integration/react'
import {Switch, Route} from 'react-router-dom'
// components
import Layout from './components/Layout/Layout'
import AuthLayout from './components/Layout/AuthLayout'

import {routes} from './routes/home'

import configStore from './configStore'

const { store, history, persistor} = configStore

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <ConnectedRouter history={history}>
          <Switch key={'switch'}>
            <Route path={routes.ROUTER_LOGIN} component={AuthLayout} />
            <Route path={routes.ROUTE_HOME_PAGE} component={Layout} />
          </Switch>
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  )
}

export default App
