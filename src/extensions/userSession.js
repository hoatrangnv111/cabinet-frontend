import {
  FORMAT_DATE_TIME_STRING,
  formatDateTimeToString,
  getNewDate
} from '../extensions/dateTime'
import stringExtensions from '../extensions/string'
import { DEFAULT_PREFIX, TAB_STYLES } from '../constants'

export const initUserInfor = (data) => {
  if (!data) return null
  return {
    code: data.code
      ? data.code
      : data.name
      ? data.name
      : initJobCode({ backSpace: '_' }),
    firstName: data.firstName,
    lastName: data.lastName,
    phoneNumber: data.phoneNumber,
    email: data.email,
    address: data.address
  }
}

const initJobCode = ({ prefix = DEFAULT_PREFIX, backSpace = '' }) => {
  return `${prefix}${backSpace}${formatDateTimeToString(
    getNewDate(),
    FORMAT_DATE_TIME_STRING
  )}`
}

export const initUserCurrentJob = (data) => {
  if (!data || data.length === 0) return []
  return data
}

export const mapUserSessionWithState = (data, initialState) => {
  const objectUserSession = initialState
  if (!data) return objectUserSession
  return {
    ...objectUserSession,
    id: data.id,
    activeStyle: data.activeStyle,
    isCollageStyle: data.isCollageStyle,
    userInfor: initUserInfor(data),
    userSessionStyles: data.userSessionStyles,
    currentJob: initUserCurrentJob(data.userSessionJobs)
  }
}

export const getItemActive = ({
  currentActive,
  data,
  isFirstDefault = true
}) => {
  if (!data || data.length === 0) return null
  if (currentActive) {
    const item = data.find((i) => i.id === currentActive)
    return item ? item : isFirstDefault ? data[0] : null
  }
  return isFirstDefault ? data[0] : null
}

export const previewUserSession = (data, isDefault = false) => {
  if (!isDefault) {
    return data
  }
  const feature =
    data.features && data.features.length > 0 ? data.features[0] : null
  return {
    featureCategoryId: data.id,
    featureId: feature?.id,
    feature: feature,
    featureProductId: null,
    featureProduct: null,
    finishListId: null,
    finishList: null
  }
}

const initTabStyle = (data, key) => {
  return {
    tabKey: key.toString(),
    tabName: `Style ${key + 1}`,
    styleItem: data
  }
}

const initTabsStyles = (data, previewStyle) => {
  const tabStyles = []
  for (let i = 0; i < TAB_STYLES; i++) {
    let styleItem = data?.styleItem ?? null
    if (!styleItem && i === 0) {
      styleItem = previewStyle
    }
    tabStyles.push(initTabStyle(styleItem, i))
  }
  return tabStyles
}

export const initUserSessionStyleItem = (style) => {
  const previewStyle = style ? previewUserSession(style) : null
  return {
    id: style?.id ?? null,
    previewStyle,
    activeTab: '0',
    tabStyles: style ? initTabsStyles(style, previewStyle) : []
  }
}

export const initUserSessionStyles = (listCurrentStyles, selectedStyle) => {
  if (!selectedStyle) return listCurrentStyles
  const styles = JSON.parse(JSON.stringify(listCurrentStyles)) || []
  if (!styles || styles.length === 0) {
    styles.push(initUserSessionStyleItem(selectedStyle))
  } else {
    const findItem = styles.find((item) => item.id === selectedStyle.id)
    if (!findItem) {
      styles.push(initUserSessionStyleItem(selectedStyle))
    }
  }
  return styles
}

export const replaceContentTab = ({
  active,
  currentStyles,
  isReplace,
  isDelete
}) => {
  if (!active) return currentStyles
  const styles = JSON.parse(JSON.stringify(currentStyles)) || []
  if (styles?.length > 0) {
    const index = styles.findIndex(({ id }) => id === active)
    if (index >= 0) {
      const currentStyle = styles[index]
      const previewData = currentStyle.previewStyle
      const activeTabKey = currentStyle.activeTab
      const tabStyles = currentStyle.tabStyles
      if (tabStyles && tabStyles.length > 0) {
        const indexTab = tabStyles.findIndex(
          ({ tabKey }) => tabKey === activeTabKey
        )
        if (indexTab >= 0) {
          tabStyles.splice(indexTab, 1, {
            ...tabStyles[indexTab],
            styleItem: isReplace
              ? previewData
              : isDelete
              ? null
              : tabStyles[indexTab].styleItem
          })
        }
      }
      styles.splice(index, 1, {
        ...styles[index],
        tabStyles: tabStyles
      })
    }
  }
  return styles
}

export const setPreviewUserSessionStyles = (
  active,
  listCurrentStyles,
  data
) => {
  if (!data || !active) return listCurrentStyles
  const styles = JSON.parse(JSON.stringify(listCurrentStyles)) || []
  if (styles && styles.length > 0) {
    const indexItem = styles.findIndex((item) => item.id === active)
    if (indexItem >= 0) {
      const currentStyle = styles[indexItem]
      const previewData = {
        ...currentStyle.previewStyle,
        ...data
      }
      styles.splice(indexItem, 1, {
        ...styles[indexItem],
        previewStyle: previewData
      })
    }
  }
  return styles
}

export const setValueUserSessionStyles = (active, listCurrentStyles, data) => {
  if (!data || !active) return listCurrentStyles
  const styles = JSON.parse(JSON.stringify(listCurrentStyles)) || []
  if (styles && styles.length > 0) {
    const indexItem = styles.findIndex((item) => item.id === active)
    if (indexItem >= 0) {
      styles.splice(indexItem, 1, {
        ...styles[indexItem],
        ...data
      })
    }
  }
  return styles
}
