export const getFeatureTitle = (feature) => {
  if (!feature) return null
  return feature.featureCategory?.name
}