import { getIn } from 'formik'
import _ from 'lodash'

export default {
  checkFieldError({ errors, touched, fieldName }) {
    return !!(getIn(errors, fieldName) && getIn(touched, fieldName))
  },

  getDefaultValueField({ data, fieldName, defaultFieldValue }) {
    if (!data || (typeof data[fieldName] !== 'boolean' && !data[fieldName])) {
      return defaultFieldValue ? defaultFieldValue : null
    }
    return data[fieldName]
  },

  getObjectValueField({ rawValue, valueField, labelField }) {
    if (rawValue) {
      return {
        ...rawValue,
        key: valueField ? rawValue[valueField] : rawValue._id,
        value: valueField ? rawValue[valueField] : rawValue._id,
        label: labelField ? rawValue[labelField] : rawValue.name
      }
    }
    return null
  },

  getListObjectValueField({ data, fieldName, labelField, valueField }) {
    if (!data || !data[fieldName]) {
      return []
    }

    return data[fieldName].map((item) =>
      this.getObjectValueField({
        rawValue: item,
        labelField: labelField,
        valueField: valueField
      })
    )
  },

  preventEnterSubmitForm(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault()
    }
  }
}
