export default {
  loadDataPager(queryClause) {
    return `
    query {
      userSessions(${queryClause.whereClause}) {
        id
        code
      }
    }    
    `
  },
  loadData(queryClause) {
    return `
    query {
      userSession(${queryClause}) {
        id
        code
        firstName
        lastName
        phoneNumber
        email
        address
        activeStyle
        isCollageStyle
        userSessionStyles {
          featureCategoryId
          featureId
          featureProductId
          finishListId
          feature {
            id
            name
            description
            images
          }
          featureProduct {
            id
            name
            images
          }
          finishList {
            id
            name
            images
          }
        }
        userSessionJobs {
          id
          productId
          propertyValues
        }
      }
    }    
    `
  },

  create(queryClause) {
    return `
    mutation {
      createUserSession(${queryClause}) {
        id
        code
      }
    }
    `
  },

  update(queryClause) {
    return `
    mutation {
      updateUserSession(${queryClause}) {
        id
        code
      }
    }    
    `
  }
}
