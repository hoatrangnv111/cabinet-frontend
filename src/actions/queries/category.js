export default {
  loadDataPager(queryClause) {
    return `
    query {
      categories(${queryClause.whereClause}) {
        id
        name
        isPublished
        isDeleted
        order
      }
    }    
    `
  },

  loadData(queryClause) {
    return `
    query {
      category(${queryClause}) {
        id
        name
        order
        parent {
          id
          name
        }
      }
    }    
    `
  },

  create(queryClause) {
    return `
    mutation {
      createCategory(${queryClause}) {
        id
      }
    }
    `
  },

  update(queryClause) {
    return `
    mutation {
      updateCategory(${queryClause}) {
        id
      }
    }    
    `
  }
}
