export default {
  loadDataPager(queryClause) {
    return `
    query {
      properties(${queryClause.whereClause}) {
        id
        key
        defaultValue
        isPublished
        isShowDefault
        order
      }
      propertiesConnection(${queryClause.whereConnectionClause}) {
        fullCount
        edges {
          cursor
        }
      }
    }    
    `
  },

  loadData(queryClause) {
    return `
    query {
      property(${queryClause}) {
        id
        key
        description
        shortDescription
        defaultValue
        isPublished
        isShowDefault
        order
      }
    }    
    `
  },

  create(queryClause) {
    return `
    mutation {
      createProperty(${queryClause}) {
        id
      }
    }
    `
  },

  update(queryClause) {
    return `
    mutation {
      updateProperty(${queryClause}) {
        id
      }
    }    
    `
  }
}
