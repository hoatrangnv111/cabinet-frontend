export default {
  loadDataPager(queryClause) {
    return `
    query {
      menuFeatureCategories {
        featureCategory {
          id
          name
          features {
            id
            name
            images
            order
          }
        }
        defaultStyle {
          id
          featureCategoryId
          featureId
          feature {
            id
            name
            description
            images
            order
          }
          featureProduct {
            id
            name
            description
            images
            order
          }
          finishList {
            id
            name
            description
            images
            order
          }
        }
      }
    }    
    `
  }
}
