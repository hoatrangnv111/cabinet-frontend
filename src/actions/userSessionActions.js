import Notifications from 'react-notification-system-redux'
import ActionTypes from './types'
import query from './queries'
import axiosProvider from './api/axiosProvider'
import { objectExtensions } from '../extensions'
import { initCurrentJob } from '../extensions/product'
import { setFormFieldError } from './formActions'
import { GRAPHQL_PATH } from '../constants'
import userSessionClause from '../clauseSearch/userSession'
import dataActions from './dataActions'
import {
  FEATURE_PRODUCT_PATH,
  FINISH_LIST_PATH,
  USER_SESSION_PATH
} from '../constants/path'
import featureProductClause from '../clauseSearch/featureProduct'
import finishListClause from '../clauseSearch/finishList'
import {
  loadFail,
  loadSuccess,
  redirectPath,
  startFetchingAction,
  startFetchingFeatureProduct,
  stopFetchingAction,
  stopFetchingFeatureProduct
} from './commonActions'
import { routes } from '../routes/home'

const userSessionActions = {
  addCurrentJob(product) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.ADD_CURRENT_JOB,
        payload: { data: initCurrentJob(product) }
      })
    }
  },
  removeCurrentJob(currentJob) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.REMOVE_CURRENT_JOB,
        payload: { data: currentJob }
      })
    }
  },
  changePropertyItem(currentJob) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.CHANGE_CURRENT_JOB,
        payload: { data: currentJob }
      })
    }
  },
  clearSuccess() {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.CLEAR_USER_SESSION
      })
    }
  },
  setFeature(data = null) {
    return async (dispatch) => {
      try {
        dispatch(startFetchingFeatureProduct())
        dispatch(
          dataActions.resetData('featureProducts', [], FEATURE_PRODUCT_PATH)
        )
        dispatch(
          dataActions.resetData(
            'searchFinishListByProduct',
            [],
            FINISH_LIST_PATH
          )
        )
        if (data) {
          dispatch(
            this.loadSuccess({
              actionType: ActionTypes.SET_FEATURE_CATEGORY,
              data: {
                featureId: data.id,
                feature: data
              }
            })
          )
          const clause = featureProductClause.initQueryGetAllFeatureProduct({
            feature: data.id
          })
          await dispatch(
            dataActions.loadDataPager(clause, FEATURE_PRODUCT_PATH)
          )
          dispatch(this.setFeatureProduct())
          dispatch(this.setFinishList())
        }
        dispatch(stopFetchingFeatureProduct())
      } catch (error) {
        dispatch(stopFetchingFeatureProduct())
      }
    }
  },
  setFeatureProduct(
    data = null,
    finishList = null,
    isDefaultFinishList = false
  ) {
    return async (dispatch) => {
      dispatch(stopFetchingFeatureProduct())
      try {
        await dispatch(
          dataActions.resetData(
            'searchFinishListByProduct',
            [],
            FINISH_LIST_PATH
          )
        )
        dispatch(
          this.loadSuccess({
            actionType: ActionTypes.SET_FEATURE_CATEGORY,
            data: {
              featureProductId: data?.id || null,
              featureProduct: data
            }
          })
        )
        if (data) {
          const clause = finishListClause.initQueryFinishLists({
            product: data.id
          })
          await dispatch(dataActions.loadDataPager(clause, FINISH_LIST_PATH))
          if (!isDefaultFinishList) {
            dispatch(this.setFinishList())
          }
        }
      } catch (error) {}
    }
  },
  setFinishList(data) {
    return async (dispatch) => {
      dispatch(
        this.loadSuccess({
          actionType: ActionTypes.SET_FEATURE_CATEGORY,
          data: {
            finishListId: data?.id || null,
            finishList: data
          }
        })
      )
    }
  },
  setActiveStyle(isCollapseStyle = false, data = null) {
    return async (dispatch) => {
      dispatch(
        this.loadSuccess({
          actionType: isCollapseStyle
            ? ActionTypes.SET_COLLAGE_STYLE
            : ActionTypes.SET_ACTIVE_STYLE,
          data
        })
      )
      if (!isCollapseStyle) {
        await dispatch(this.loadSessionActive())
      }
    }
  },
  loadSessionActive() {
    return async (dispatch, getState) => {
      dispatch(
        dataActions.resetData('featureProducts', [], FEATURE_PRODUCT_PATH)
      )
      dispatch(
        dataActions.resetData('searchFinishListByProduct', [], FINISH_LIST_PATH)
      )
      const stateUserSession = await getState().userSession
      if (stateUserSession) {
        //fínd item active
        const userSessionStyles = stateUserSession.userSessionStyles || []
        const userSessionStyleActive = userSessionStyles.find(
          (item) => item.id === stateUserSession.activeStyle
        )
        if (userSessionStyleActive) {
          const { previewStyle } = userSessionStyleActive
          const clause = featureProductClause.initQueryGetAllFeatureProduct({
            feature: previewStyle.featureId
          })
          await dispatch(
            dataActions.loadDataPager(clause, FEATURE_PRODUCT_PATH)
          )
          if (previewStyle.featureProductId) {
            const clause = finishListClause.initQueryFinishLists({
              product: previewStyle.featureProductId
            })
            await dispatch(dataActions.loadDataPager(clause, FINISH_LIST_PATH))
            if (previewStyle.finishList) {
              await dispatch(this.setFinishList(previewStyle.finishList))
            }
          }
        }
      }
    }
  },
  loadSuccess({ actionType, data }) {
    return (dispatch) => {
      dispatch({
        type: actionType,
        payload: { data: data }
      })
    }
  },
  loadData(objectId, pathQuery) {
    return async (dispatch) => {
      try {
        this.clearSuccess()
        //console.log('reload suceesss')
        if (objectId) {
          //console.log('has object.....', objectId)
          const clause = userSessionClause.initQueryUniqueUserSession(objectId)

          const queryData = query[pathQuery].loadData(clause)

          const response = await axiosProvider().post(GRAPHQL_PATH, {
            query: `${queryData}`,
            variables: null
          })

          if (response.status === 200 && !response.data.errors) {
            await dispatch(
              this.loadSuccess({
                actionType: ActionTypes.FETCH_LOAD_USER_SESSION,
                data: response?.data?.data?.userSession
              })
            )

            dispatch(this.loadSessionActive())
          } else {
            dispatch(
              Notifications.error({
                title: `Error`,
                message: `${response.data.errors[0].message}`
              })
            )
          }
        }
      } catch (error) {
        dispatch(
          Notifications.error({
            title: `Error`,
            message: `${error.toString()}`
          })
        )
      }
    }
  },
  /**
   *
   * @param currentSession
   * @param userInfor
   * @returns {function(...[*]=)}
   */
  saveJob(currentSession, userInfor) {
    return async (dispatch) => {
      if (currentSession) {
        const session = {
          ...currentSession,
          userInfor: userInfor
        }

        await dispatch(
          this.loadSuccess({
            actionType: ActionTypes.SET_USER_SESSION,
            data: session
          })
        )

        const clauseSeach = userSessionClause.initQueryCreateOrUpdateUserSession(
          session
        )
        if (session.id) {
          dispatch(
            this.updateData({
              clause: clauseSeach
            })
          )
        } else {
          dispatch(
            this.createData({
              clause: clauseSeach
            })
          )
        }
      }
    }
  },
  /**
   * create data
   * @param clause
   * @param pathQuery
   * @param pathRedirect
   * @param enableGoBack
   * @returns {Function}
   */
  createData({ clause, userId }) {
    return async (dispatch) => {
      dispatch(startFetchingAction())
      try {
        const queryData = query[USER_SESSION_PATH].create(clause)
        const response = await axiosProvider().post(GRAPHQL_PATH, {
          query: `${queryData}`,
          variables: null
        })
        if (response.status === 200 && !response.data.errors) {
          //update redux
          const result = response?.data?.data?.createUserSession
          dispatch(redirectPath(`${routes.ROUTE_USER_SESSION}/${result?.id}`))
        } else {
          dispatch(
            Notifications.error({
              title: `Error`,
              message: `${response.data.errors[0].message}`
            })
          )
          dispatch(loadFail(response.data.errors[0]))
        }
      } catch (error) {
        dispatch(
          Notifications.error({
            title: `Error`,
            message: `${error.toString()}`
          })
        )
      }
      dispatch(stopFetchingAction())
    }
  },

  /**
   * update data
   * @param clause
   * @param pathQuery
   * @param pathRedirect
   * @param enableGoBack
   * @returns {Function}
   */
  updateData({ clause, pathQuery, pathRedirect = null, enableGoBack = true }) {
    return async (dispatch) => {
      dispatch(startFetchingAction())
      try {
        const queryData = query[USER_SESSION_PATH].update(clause)

        const response = await axiosProvider().post(GRAPHQL_PATH, {
          query: `${queryData}`,
          variables: null
        })

        if (response.status === 200 && !response.data.errors) {
          //update redux
          console.log('update jobs.................')
        } else {
          dispatch(
            Notifications.error({
              title: `Error`,
              message: `${response.data.errors[0].message}`
            })
          )

          dispatch(loadFail(response.data.errors[0]))
        }
      } catch (error) {
        dispatch(
          Notifications.error({
            title: `Error`,
            message: `${error.toString()}`
          })
        )
        dispatch(loadFail(error.response))
      }
      dispatch(stopFetchingAction())
    }
  },

  setActiveTab(tabKey) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.ACTIVE_TAB,
        payload: { data: { activeTab: tabKey } }
      })
    }
  },

  replaceContentTab(tabKey) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.REPLACE_CONTENT_TAB,
        payload: { isReplace: true, isDelete: false }
      })
    }
  },

  deleteContentTab(tabKey) {
    return (dispatch) => {
      dispatch({
        type: ActionTypes.REPLACE_CONTENT_TAB,
        payload: { isReplace: false, isDelete: true }
      })
    }
  }
}

export default userSessionActions
