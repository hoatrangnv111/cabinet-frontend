import {createSelector} from 'reselect'
import { FINISH_LIST_PATH} from '../constants/path'

const getData = state => state.data ? state.data : null

const getFinishLists = (data) => {
  if (!data) return []
  return data.searchFinishListByProduct  ? data.searchFinishListByProduct  || [] : []
}

export const selectFinishLists = () =>
  createSelector(
    [getData],
    data => {
      return getFinishLists(data ? data.get(FINISH_LIST_PATH) : null)
    }
  )