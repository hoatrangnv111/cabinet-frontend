import { createSelector } from 'reselect'

const getAuth = (state) => state.auth

export const selectUserLogin = () => createSelector([getAuth], (auth) => auth?.user)

export const selectTokenExpire = () =>
  createSelector([getAuth], (auth) => auth?.expiresIn)
