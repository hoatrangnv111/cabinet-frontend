import {createSelector} from 'reselect'
import {CATEGORY_PATH} from '../constants/path'

const getData = state => state.data ? state.data : null

const getCategories = (data) => {
  if (!data) return []
  return data.categories  ? data.categories  || [] : []
}

export const selectCategories = () =>
  createSelector(
    [getData],
    data => {
      return getCategories(data ? data.get(CATEGORY_PATH) : null)
    }
  )