import { createSelector } from 'reselect'
import { FEATURE_CATEGORY_PATH } from '../constants/path'

const getData = (state) => (state.data ? state.data : null)

const getFeatureCategories = (data) => {
  return data?.menuFeatureCategories ?? []
}

export const selectFeatureCategories = () =>
  createSelector([getData], (data) => {
    return getFeatureCategories(data ? data.get(FEATURE_CATEGORY_PATH) : null)
  })
