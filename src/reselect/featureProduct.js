import {createSelector} from 'reselect'
import {FEATURE_PRODUCT_PATH} from '../constants/path'

const getData = state => state.data ? state.data : null

const getFeatureProducts = (data) => {
  if (!data) return []
  return data.featureProducts  ? data.featureProducts  || [] : []
}

const getLoadingFeatureProduct = (data) => {
  if (!data) return false
  return data.isLoading  ? data.isLoading  || false : false
}

export const selectFeatureProducts = () =>
  createSelector(
    [getData],
    data => {
      return getFeatureProducts(data ? data.get(FEATURE_PRODUCT_PATH) : null)
    }
  )

export const selectLoadingFeatureProduct = () =>
  createSelector(
    [getData],
    data => {
      return getLoadingFeatureProduct(data ? data.get(FEATURE_PRODUCT_PATH) : null)
    }
  )