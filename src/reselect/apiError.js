import { createSelector } from 'reselect'

const getApiError = (state) => state.apiError

export const selectApiErrorCode = () =>
  createSelector([getApiError], (error) => error?.code)

export const selectApiErrorMessage = () =>
  createSelector([getApiError], (error) => error?.message)
