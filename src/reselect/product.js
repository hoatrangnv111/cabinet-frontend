import {createSelector} from 'reselect'
import {PRODUCT_PATH} from '../constants/path'

const getData = state => state.data ? state.data : null

const getProductByCategory = (data) => {
  if (!data) return []
  return data.searchProductByCategory  ? data.searchProductByCategory  || [] : []
}

const getLoadingProduct = (data) => {
  if (!data) return true
  return data.isLoading || false
}

export const selectLoadingProduct = () =>
  createSelector(
    [getData],
    data => {
      return getLoadingProduct(data ? data.get(PRODUCT_PATH) : null)
    }
  )

export const selectProductByCategory = () =>
  createSelector(
    [getData],
    data => {
      return getProductByCategory(data ? data.get(PRODUCT_PATH) : null)
    }
  )