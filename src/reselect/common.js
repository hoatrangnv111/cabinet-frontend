import { createSelector } from 'reselect'

const getFPLoading = (state) => state.common?.fpLoading

export const selectFPLoading = () =>
  createSelector([getFPLoading], (loading) => !!loading)
