import {createSelector} from 'reselect'
import {FEATURE_PATH} from '../constants/path'

const getData = state => state.data ? state.data : null

const getFeatures = (data) => {
  if (!data) return []
  return data.features  ? data.features  || [] : []
}

export const selectFeatures = () =>
  createSelector(
    [getData],
    data => {
      return getFeatures(data ? data.get(FEATURE_PATH) : null)
    }
  )