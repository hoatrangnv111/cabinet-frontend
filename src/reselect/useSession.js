import { createSelector } from 'reselect'
import { USER_SESSION_PATH } from '../constants/path'
import { initUserSessionStyleItem } from '../extensions/userSession'

const getData = (state) => (state.data ? state.data : null)

const getCurrentSession = (state) =>
  state.userSession ? state.userSession : null

const getUserSessions = (data) => {
  if (!data) return []
  return data.userSessions ? data.userSessions || [] : []
}

export const selectUserSessions = () =>
  createSelector([getData], (data) => {
    return getUserSessions(data ? data.get(USER_SESSION_PATH) : null)
  })

export const selectCurrentSession = () =>
  createSelector([getCurrentSession], (data) => {
    return data || null
  })

export const selectUserInfor = () =>
  createSelector([getCurrentSession], (data) => {
    return data?.userInfor
  })

export const selectUserSessionStyles = () =>
  createSelector([getCurrentSession], (data) => {
    return data?.userSessionStyles || []
  })

export const selectActiveUserSessionStyle = () =>
  createSelector([getCurrentSession], (data) => {
    return {
      activeStyle: data?.activeStyle || null,
      isCollageStyle: data?.isCollageStyle || false
    }
  })

export const selectActiveItemStyle = (item) =>
  createSelector([getCurrentSession], (data) => {
    const result = initUserSessionStyleItem(item)
    if (!data) return result
    const userSessionStyles = data.userSessionStyles || []
    if (!userSessionStyles || userSessionStyles.length === 0) return result
    const findItem = userSessionStyles.find((s) => s.id === item.id)
    return findItem ? findItem : result
  })
