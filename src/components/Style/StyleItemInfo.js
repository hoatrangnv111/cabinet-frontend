import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import classnames from 'classnames'
import List from 'antd/lib/list'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
// actions
// clause
// reselect
import {
  selectFeatureProducts,
  selectLoadingFeatureProduct
} from '../../reselect/featureProduct'
import { selectFinishLists } from '../../reselect/finishList'
// constants
import { UPLOAD_PRODUCT } from '../../constants'
import { COL_12, COL_24, COL_6, DEFAULT_GUTTER } from '../../constants/layout'
// components
import CurrentTypeItem from '../CurrentTypeItem'
import { generateImages } from '../../extensions/image'
import userSessionActions from '../../actions/userSessionActions'
import { usePrevious } from '../../hooks/usePrevious'
import { selectFPLoading } from '../../reselect/common'
import { Spin } from 'antd'
import Skeleton from '../Skeleton'

const StyleItemInfo = ({
  features,
  feature,
  featureProduct: currentFeatureProduct,
  finishList: currentFinishList,
  handleClickFeature
}) => {
  const [firstLoad, setIsFirstLoad] = useState(true)
  const featureProducts = useSelector(selectFeatureProducts())

  const finishLists = useSelector(selectFinishLists())

  const loadingFP = useSelector(selectFPLoading())

  const dispatch = useDispatch()

  const setCurrentFeatureProductCallback = useCallback(
    (data, finishList, isDefaultFinishList) =>
      dispatch(
        userSessionActions.setFeatureProduct(
          data,
          finishList,
          isDefaultFinishList
        )
      ),
    [dispatch]
  )

  const setCurrentFinishListCallback = useCallback(
    (data) => dispatch(userSessionActions.setFinishList(data)),
    [dispatch]
  )

  const handleClickFeatureProduct = (fpSelected) => {
    setIsFirstLoad(false)
    if (fpSelected) {
      if (fpSelected.id !== currentFeatureProduct?.id) {
        setCurrentFeatureProductCallback(fpSelected, currentFinishList)
      }
    } else {
      setCurrentFeatureProductCallback(null, currentFinishList)
    }
  }

  const handleClickFinishList = (flSelected) => {
    setIsFirstLoad(false)
    if (flSelected) {
      if (flSelected.id !== currentFinishList?.id) {
        setCurrentFinishListCallback(flSelected)
      }
    } else {
      setCurrentFinishListCallback(null)
    }
  }

  const previousFProduct = usePrevious(featureProducts)

  const previousFinishList = usePrevious(finishLists)

  // useEffect(() => {
  //   const hasChangeFProduct =
  //     JSON.stringify(previousFProduct) !== JSON.stringify(featureProducts)

  //   if (hasChangeFProduct && featureProducts?.length > 0) {
  //     setCurrentFeatureProductCallback(
  //       featureProducts[0],
  //       currentFinishList,
  //       !!firstLoad
  //     )
  //     setIsFirstLoad(false)
  //   }
  // }, [feature?.id, featureProducts])

  // useEffect(() => {
  //   const hasChangeFinishList =
  //     JSON.stringify(previousFinishList) !== JSON.stringify(finishLists)

  //   if (hasChangeFinishList && finishLists?.length > 0) {
  //     // setCurrentFinishListCallback(finishLists[0])
  //   }
  // }, [currentFeatureProduct?.id, finishLists])

  return (
    <Row gutter={DEFAULT_GUTTER}>
      <Col span={COL_24} xl={COL_12}>
        <Row gutter={DEFAULT_GUTTER}>
          {features?.map((item, index) => (
            <Col span={COL_24} xl={COL_24} key={index}>
              <CurrentTypeItem
                key={index}
                title={item.name}
                image={generateImages({
                  object: item,
                  filed: 'images',
                  dir: UPLOAD_PRODUCT
                })}
                description={item.description}
                customizeClass={classnames('h-180x', {
                  active: item?.id === feature?.id
                })}
                header={<p className={'font-weight-bold'}>{item.name}</p>}
                onClick={() => {
                  handleClickFeature(item)
                }}
                hoverable
              />
            </Col>
          ))}
        </Row>
      </Col>
      <Col span={COL_24} xl={COL_6}>
        {loadingFP ? (
          <Skeleton hasImage={false} count={2} />
        ) : (
          <List
            className={'cbn-list-items'}
            itemLayout='horizontal'
            dataSource={featureProducts}
            renderItem={(item) => (
              <List.Item
                className={classnames('mb-3 h-82x', {
                  active: !firstLoad && item?.id == currentFeatureProduct?.id
                })}
                onClick={() => {
                  handleClickFeatureProduct(item)
                }}
              >
                <List.Item.Meta
                  title={<a>{item.name}</a>}
                  //description={<p>{item.description}</p>}
                />
              </List.Item>
            )}
          />
        )}
      </Col>
      <Col span={COL_24} xl={COL_6}>
        <Row gutter={DEFAULT_GUTTER}>
          {finishLists.map((item, index) => (
            <Col span={COL_24} xl={COL_24} key={index}>
              <CurrentTypeItem
                key={index}
                title={item.name}
                image={generateImages({
                  object: item,
                  filed: 'images',
                  dir: UPLOAD_PRODUCT
                })}
                //description={item.description}
                //type={'small'}
                customizeClass={classnames('h-82x cbn-style', {
                  active: item?.id == currentFinishList?.id
                })}
                header={<p className={'font-weight-bold'}>{item.name}</p>}
                onClick={() => {
                  handleClickFinishList(item)
                }}
                hoverable
              />
            </Col>
          ))}
        </Row>
      </Col>
    </Row>
  )
}

export default StyleItemInfo
