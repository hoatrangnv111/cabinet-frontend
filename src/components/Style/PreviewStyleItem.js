import React, { Fragment, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import Row from 'antd/lib/row'
import Button from 'antd/lib/button'
import Divider from 'antd/lib/divider'
import Col from 'antd/lib/col'
import Modal from 'antd/lib/modal'
import { ExclamationCircleOutlined } from '@ant-design/icons'
// actions
import userSessionActions from '../../actions/userSessionActions'
// extensions
import { generateImages } from '../../extensions/image'
// constants
import { COL_10, COL_24, COL_7, DEFAULT_GUTTER } from '../../constants/layout'
import { UPLOAD_PRODUCT } from '../../constants'
import { CONFIRM_MODAL, ConfirmReplaceContent } from '../../constants/message'
// components
import CustomizeCard from '../CustomizeCard'

const PreviewStyleItem = ({ isPreview, previewStyle, changeContentStyle }) => {
  const dispatch = useDispatch()

  const replaceContentStyle = useCallback(
    () => dispatch(userSessionActions.replaceContentTab()),
    [dispatch]
  )

  const deleteContentStyle = useCallback(
    () => dispatch(userSessionActions.deleteContentTab()),
    [dispatch]
  )

  const handleClick = () => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: isPreview ? <ConfirmReplaceContent /> : CONFIRM_MODAL,
      okText: 'Yes',
      cancelText: 'Cancel',
      onOk: () => {
        changeContentStyle()
        if (isPreview) {
          replaceContentStyle()
        } else {
          deleteContentStyle()
        }
      }
    })
  }
  return (
    <Row gutter={DEFAULT_GUTTER}>
      <Col span={COL_24} xl={COL_7}>
        <img
          src={generateImages({
            object: previewStyle?.feature,
            filed: 'images',
            dir: UPLOAD_PRODUCT
          })}
          className='img-fluid'
        />
      </Col>
      <Col span={COL_24} xl={COL_10}>
        <div className={'h-134x'}>
          <Divider orientation='left'>{previewStyle?.feature?.name}</Divider>
          {previewStyle?.featureProduct ? (
            <Divider>{previewStyle?.featureProduct?.name}</Divider>
          ) : null}
          {previewStyle?.finishList ? (
            <Divider orientation='right'>
              {previewStyle?.finishList?.name}
            </Divider>
          ) : null}
        </div>
        <Button
          className='mb-1'
          danger={!isPreview}
          onClick={() => {
            handleClick()
          }}
        >
          {isPreview ? 'Replace content' : 'Delete content'}
        </Button>
      </Col>
      <Col span={COL_24} xl={COL_7}>
        <img
          src={generateImages({
            object: previewStyle?.finishList,
            filed: 'images',
            dir: UPLOAD_PRODUCT
          })}
          className='img-fluid'
        />
      </Col>
    </Row>
  )
}

export default PreviewStyleItem
