import React, { useCallback, useState } from 'react'
import Tabs from 'antd/lib/tabs'
import PreviewStyleItem from './PreviewStyleItem'
import CustomizeCard from '../CustomizeCard'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import { useDispatch } from 'react-redux'
import userSessionActions from '../../actions/userSessionActions'
import { COL_12, COL_24, DEFAULT_GUTTER } from '../../constants/layout'

const { TabPane } = Tabs

const TabsStyle = ({ data, activeTab, previewStyle }) => {
  const dispatch = useDispatch()

  const setActiveTab = useCallback(
    (activeTab) => dispatch(userSessionActions.setActiveTab(activeTab)),
    [dispatch]
  )

  const onChange = (key) => {
    if (key !== activeTab) {
      setActiveTab(key)
    }
  }

  const changeContentStyle = () => {}
  return (
    <Tabs
      onChange={onChange}
      type='card'
      size={'small'}
      activeKey={data.activeTab}
    >
      {data && data.length > 0
        ? data.map((item, key) => {
            return (
              <TabPane tab={item.tabName} key={item.tabKey}>
                <Row gutter={DEFAULT_GUTTER} className={'mb-3'}>
                  <Col span={COL_24} xl={COL_12}>
                    <CustomizeCard>
                      <PreviewStyleItem
                        previewStyle={item.styleItem}
                        changeContentStyle={changeContentStyle}
                      />
                    </CustomizeCard>
                  </Col>
                  <Col span={COL_24} xl={COL_12}>
                    <CustomizeCard>
                      <PreviewStyleItem
                        previewStyle={previewStyle}
                        activeTab={activeTab}
                        isPreview={true}
                        changeContentStyle={changeContentStyle}
                      />
                    </CustomizeCard>
                  </Col>
                </Row>
              </TabPane>
            )
          })
        : null}
    </Tabs>
  )
}

export default TabsStyle
