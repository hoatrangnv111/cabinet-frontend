// react
import React, { useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Card from 'antd/lib/card'
import Col from 'antd/lib/col'
import { DownOutlined, UpOutlined } from '@ant-design/icons'
// HOOK
// actions
import userSessionActions from '../../actions/userSessionActions'
// reselect
import {
  selectActiveItemStyle,
  selectActiveUserSessionStyle
} from '../../reselect/useSession'
// clause search
// constants
import { COL_24 } from '../../constants/layout'
// extensions
// components
import StyleItemInfo from './StyleItemInfo'
import TabsStyle from './TabsStyle'

const BlockStyle = ({ data, defaultStyle, handleToggleCollage }) => {
  const dispatch = useDispatch()

  const [] = useState(true)

  const { activeStyle, isCollageStyle } = useSelector(
    selectActiveUserSessionStyle()
  )

  const isActiveItem = data?.id === activeStyle

  const isShowElement = isActiveItem && isCollageStyle

  const { previewStyle, activeTab, tabStyles } =
    useSelector(selectActiveItemStyle(defaultStyle)) || null

  const setCurrentFeatureCallback = useCallback(
    (item) => dispatch(userSessionActions.setFeature(item)),
    [dispatch]
  )

  // useEffect(() => {
  //   if(isActiveItem && isCollageStyle){
  //     setFeature(feature)
  //     if(featureProduct){
  //       setFeatureProduct(featureProduct, finishList)
  //     }
  //   }
  // }, [isActiveItem, isCollageStyle])

  const handleClickFeature = (selected) => {
    if (selected && selected.id !== previewStyle?.feature?.id) {
      if (isActiveItem && isCollageStyle) {
        setCurrentFeatureCallback(selected)
      }
    }
  }

  return data ? (
    <Card
      id={data?.name}
      title={<div className='text-left'>{data?.name}</div>}
      className={'card mb-3'}
      extra={
        <a
          onClick={() => {
            handleToggleCollage(defaultStyle)
          }}
        >
          {isShowElement ? <DownOutlined /> : <UpOutlined />}
        </a>
      }
      size={'small'}
      type='inner'
    >
      <Col span={COL_24}>
        <TabsStyle
          previewStyle={previewStyle}
          activeTab={activeTab}
          data={tabStyles}
        />
      </Col>
      {!isShowElement ? null : (
        <StyleItemInfo
          features={data.features || []}
          feature={previewStyle?.feature}
          featureProduct={previewStyle?.featureProduct}
          finishList={previewStyle?.finishList}
          handleClickFeature={handleClickFeature}
        />
      )}
    </Card>
  ) : null
}

export default BlockStyle
