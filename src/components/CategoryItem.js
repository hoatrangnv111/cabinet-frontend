import React from 'react'
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Card from "antd/lib/card";
import Empty from "antd/lib/empty";
// constants
import {COL_12, COL_4, COL_6, COL_8, DEFAULT_GUTTER} from "../constants/layout";
// extensions
import {getImageProduct} from "../extensions/image";
// components
import CustomizeCard from "./CustomizeCard";

const CategoryItem = (props) => {

  const {data, handleClickCategory} = props

  return data ? (
    <div>
      <h6 className='font-weight-bold text-dark'>{data?.categoryName}</h6>
      <Row gutter={DEFAULT_GUTTER}>
        {data?.products?.length > 0
          ? data.products.map((product, index) => (
            <Col span={COL_12} md={COL_8} lg={COL_6} xl={COL_4} key={index}>
              <div
                className='cursor-pointer'
                onClick={() => handleClickCategory(product)}
              >
                <CustomizeCard
                  customizeClass='mb-3 cbn-card__category'
                  cover={
                    <div className='w-xl-100 cbn-type-image px-2 py-2 px-xl-0 py-xl-0'>
                      <img src={getImageProduct(product)} className='img-fluid' alt={'cate-item'}/>
                    </div>
                  }
                >
                  <Card.Meta className='text-center' title={product?.name}/>
                </CustomizeCard>
              </div>
            </Col>
          ))
          : <Empty />
        }
      </Row>
    </div>
  ) : null
}

export default CategoryItem