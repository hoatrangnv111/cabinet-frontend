import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Card from 'antd/lib/card'

const CustomizeCard = (props) => {
  const {id, customizeClass, title, cover, extra,hoverable, children, actions, onClick } = props
  return (
    <Card
      id={id}
      className={classNames('cbn-type-card card', customizeClass)}
      title={title}
      cover={cover}
      actions={actions}
      extra={extra}
      onClick={() => {if(onClick) onClick()}}
      hoverable={hoverable}
    >
      {children}
    </Card>
  )
}

CustomizeCard.propTypes = {
  customizeClass: PropTypes.string,
  title: PropTypes.any,
  cover: PropTypes.any,
  children: PropTypes.any,
  actions: PropTypes.array
}

export default CustomizeCard
