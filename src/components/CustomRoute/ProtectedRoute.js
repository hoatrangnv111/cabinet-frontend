import React from 'react'
// lib
import { Redirect, Route, withRouter } from 'react-router-dom'
// component
import AccessDenied from '../AccessDenied'
// routes
import { routes } from '../../routes/home'
import { getCookie } from '../../extensions/cookie'
import { COOKIE_AUTH } from '../../constants'

const ProtectedRoute = ({
  component: Component,
  resource,
  action,
  auth,
  config,
  isMobile,
  ...rest
}) => {
  const permission = getCookie(COOKIE_AUTH)

  console.log('permission.....', permission)
  if (permission) {
    return (
      <Route
        {...rest}
        render={(matchProps) => (
          <Component {...matchProps} auth={auth} isMobile={isMobile} />
        )}
      />
    )
  }

  console.log('must redirect...')
  return (
    <Redirect
      to={`${routes.ROUTER_LOGIN}?redirectURL=${rest.location.pathname}`}
    />
  )
}

export default withRouter(ProtectedRoute)
