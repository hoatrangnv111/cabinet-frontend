import AntSkeleton from 'antd/lib/skeleton'
import React from 'react'
import Space from 'antd/lib/space'
import Divider from 'antd/lib/divider'

import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import {
  COL_10,
  COL_12,
  COL_14,
  COL_16,
  COL_24,
  COL_8
} from '../constants/layout'

const Skeleton = (props) => {
  const { count = 3, hasImage } = props

  return (
    <React.Fragment>
      {new Array(count).fill(0).map((item, index) => (
        <Row key={index}>
          {hasImage ? (
            <Col span={COL_24} md={COL_10}>
              <AntSkeleton.Image />
            </Col>
          ) : null}

          <Col span={COL_24} md={hasImage ? COL_14 : COL_24}>
            <AntSkeleton />
          </Col>
        </Row>
      ))}
    </React.Fragment>
  )
}

export default Skeleton
