import React from 'react'
import {withFormik, FieldArray} from 'formik'
import * as Yup from 'yup'
import Form from "antd/lib/form";
//extensions
import {
  yupExtensions,
  formikExtensions,
  stringExtensions
} from '../../extensions'
import {getPropertiesDefault} from '../../extensions/product'
import {DEFAULT_FORM_LAYOUT} from "../../constants/layout";
//components
import InputPropertiesProduct from "../InputPropertiesProduct"
import ErrorMessage from "./ErrorMessage";

const {Item} = Form

const customFormik = withFormik({
  enableReinitialize: true,
  displayName: 'FormProperties',
  validationSchema: Yup.object({
    id: yupExtensions.stringRequired,
    productId: yupExtensions.stringRequired,
    properties: yupExtensions.array
      .test({
        name: 'check-min-max',
        test: function(properties) {
          if (!properties || properties.length <= 1) {
            return true
          }
          // // 2.1. check empty
          // const indexEmpty = properties.findIndex(
          //   (property, index) => !property.value
          // )
          // if (indexEmpty >= 0) {
          //   return this.createError({
          //     path: `properties[${indexEmpty}]`,
          //     message:
          //       'Properties is not empty.'
          //   })
          // }

          // 2.2. check min max
          const invalidProperty = properties.findIndex(
            (property) => property.value < property.minValue || property.value > property.maxValue
          )

          if (invalidProperty >= 0) {
            return this.createError({
              path: `properties[${invalidProperty}]`,
              message:
                'Properties must be greater than min value and smaller than max value.'
            })
          }
          return true
        }
      })
  }),
  mapPropsToValues: ({data, minimize}) => ({
    id: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'id'
    }),
    productId: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'productId'
    }),
    properties: minimize
      ? getPropertiesDefault(data)
      : formikExtensions.getDefaultValueField({
        data,
        fieldName: 'properties',
        defaultFieldValue: []
      })
  }),
  handleSubmit: (values, {props}) => {
    props.handleSubmit(values)
  }
})

const FormProperties = (props) => {
  const {
    values,
    labelInput,
    formLayout = DEFAULT_FORM_LAYOUT,
    minimize = false,
    setFieldTouched,
    setFieldValue,
    errors
  } = props

  return (
    <FieldArray
      name='properties'
      render={() => {
        return (
          values.properties.length > 0
            ? values.properties.map((item, index) => (
              <Item
                key={index}
                className='text-uppercase mb-0'
                label={minimize ? stringExtensions.getFirstCharacter(item.shortDescription) : item.key}
                {...formLayout}
              >
                <InputPropertiesProduct
                  currentJobId={values?.id}
                  data={item}
                  onChange={(value) => {
                    setFieldValue(`properties[${index}].value`, value)
                  }}
                  onBlur={() => {
                    setFieldTouched(`properties[${index}]`, true)
                  }}
                />
                <ErrorMessage fieldName={`properties${index}`} />
              </Item>
            ))
            : null
        )
      }}
    />
  )
}

export default customFormik(FormProperties)