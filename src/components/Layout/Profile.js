// import React from 'react'
import React, {useState, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import Dropdown from 'antd/lib/dropdown'
import Menu from 'antd/lib/menu'
import Card from 'antd/lib/card'
import Button from 'antd/lib/button'
import Col from 'antd/lib/col'
import Row from 'antd/lib/row'
import {Link} from 'react-router-dom'
// action
import authActions from '../../actions/authActions'
import {redirectPath} from "../../actions/commonActions";
//routes
import {routes} from "../../routes/home";
//reselect
import {selectUserLogin} from '../../reselect/auth';
import {selectUserSessions} from "../../reselect/useSession";
// component
import CustomModal from '../../components/CustomModal'

const menu = ({jobs = [], onLogout}) => (
  <Menu>
    <Menu.Item onClick={() => onLogout()} key={0}>
      Logout
    </Menu.Item>
    {
      jobs && jobs.length > 0
        ?
        jobs.map((job) => {
          return (
            <Menu.Item
              key={Math.random()}
            >
              {/*<Link*/}
              {/*  to={`${routes.ROUTE_USER_SESSION}/${job.id}`}*/}
              {/*  //target="_blank"*/}
              {/*  className={'text-decoration-none'}*/}
              {/*>*/}
              {/*  {job.code}*/}
              {/*</Link>*/}
              {job.code}
            </Menu.Item>
          )
        })
    : null
    }
  </Menu>
)

const Profile = () => {

  const user = useSelector(selectUserLogin())

  const userSessions = useSelector(selectUserSessions())

  // map dispatch from redux to local
  const dispatch = useDispatch()

  const onLogout = useCallback(() => dispatch(authActions.logout()), [dispatch])

  return (
    <div className='float-right d-none d-md-block w-100 text-right'>
      <Dropdown
        overlay={menu({jobs: userSessions, onLogout})}
      >
        <a
          onClick={(event) => event.preventDefault()}
        >
          Hi, {user ? user.firstName : ''}
        </a>
      </Dropdown>
    </div>
  )
}

export default Profile