import React from 'react'
import { useSelector } from 'react-redux'
import Menu from 'antd/lib/menu'
import classNames from 'classnames'
import { selectFeatureCategories } from '../../reselect/featureCategory'
import CustomizeCard from '../CustomizeCard'
import ElementScroll from '../ElementScroll'

const Sidebar = ({ className, handleSaveJob }) => {
  const featureCategories = useSelector(selectFeatureCategories())
  return (
    <div className={classNames('cbn-sidebar cbn-sidebar-left', className)}>
      <CustomizeCard>
        <ul className={'cbn-menu-sidebar'}>
          <li key={Math.random()} onClick={handleSaveJob}>
            {/*<ElementScroll*/}
            {/*  name={`save-job`}*/}
            {/*  scrollToTop*/}
            {/*  onClick={handleSaveJob}*/}
            {/*>*/}
            {/*  Save Jobs*/}
            {/*</ElementScroll>*/}
            Save Jobs
          </li>
          {featureCategories && featureCategories.length > 0
            ? featureCategories.map((item, key) => {
                const fcName = item?.featureCategory?.name
                return (
                  <li key={Math.random()}>
                    <ElementScroll
                      name={`${fcName?.toLowerCase()}`}
                      offset={-20}
                      scrollTo={fcName}
                    >
                      {fcName}
                    </ElementScroll>
                  </li>
                )
              })
            : null}
          <li key={Math.random()}>
            <ElementScroll
              name={'cate-scroll'}
              scrollTo={'cat-list'}
              offset={-20}
            >
              Cabinet list
            </ElementScroll>
          </li>
          <li key={Math.random()}>
            <ElementScroll
              name={'job-scroll'}
              scrollTo={'job-list'}
              offset={-20}
            >
              My job
            </ElementScroll>
          </li>
        </ul>
      </CustomizeCard>
    </div>
  )
}

export default Sidebar
