import React from 'react'
import Col from "antd/lib/col";
import Row from "antd/lib/row";
import {COL_12, COL_16, COL_18, COL_24, COL_6, COL_8, DEFAULT_GUTTER} from "../constants/layout";
import {getImageFeature} from "../extensions/image";
import CustomizeCard from "./CustomizeCard";
import classNames from "classnames";

const CurrentTypeItem = ({customizeClass, image, title, header, description, hiddenImage = false, hoverable = false, type, onClick}) => {
  return (
    <CustomizeCard
      customizeClass={classNames('mb-3', customizeClass)}
      onClick={() => {if(onClick) onClick()}}
      hoverable={hoverable}
    >
      <Row gutter={DEFAULT_GUTTER}>
        {
          !hiddenImage ? (
            <Col span={COL_24} md={COL_8} lg={type === 'small' ? COL_12 : COL_8}>
              <div className={type === 'small' ? '' : 'cbn-type-image'}>
                <img src={image} className='img-fluid' />
              </div>
            </Col>
          ) : null
        }
        <Col span={COL_24} md={COL_16} lg={type === 'small' ? COL_12 : COL_16}>
          {
            header ? header : (
              <h6 className={'mb-2 font-weight-bold'}>
                {title}
              </h6>
            )
          }
          <div className='mb-0'>
            {description}
          </div>
        </Col>
      </Row>
    </CustomizeCard>
  )
}

export default CurrentTypeItem