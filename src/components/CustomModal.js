import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'antd/lib/modal'
import {DEFAULT_WIDTH_MODAL} from "../constants";


const CustomModal = (props) => {
  const {
    isOpen,
    title,
    width = DEFAULT_WIDTH_MODAL,
    cancelButton,
    acceptButton,
    handleAccept,
    handleCancel
  } = props

  return (
    <Modal
      width={width}
      visible={isOpen}
      cancelText={cancelButton}
      okText={acceptButton}
      onOk={handleAccept}
      onCancel={handleCancel}
      title={title}
      centered={true}
      closable={true}
    >
      {props.children}
    </Modal>
  )
}

CustomModal.propTypes = {
  isOpen: PropTypes.bool,
  title: PropTypes.any,
  cancelButton: PropTypes.any,
  acceptButton: PropTypes.any,
  handleAccept: PropTypes.func,
  handleCancel: PropTypes.func
}

export default CustomModal
