import React from 'react'
import Row from "antd/lib/row";
import Col from "antd/lib/col";
// constants
import {COL_12, COL_15, COL_24, COL_9, DEFAULT_GUTTER} from "../constants/layout";
// extensions
import {getLabelProperties, getPropertiesDefault} from "../extensions/product";
import {getImageProduct} from "../extensions/image";
// components
import InputPropertiesProduct from './InputPropertiesProduct'
import FormProperties from './Form/FormProperties'

const JobItem = (props) => {
  const {data} = props
  return data ? (
    <div className='job-item' key={data.id}>
      <Row gutter={DEFAULT_GUTTER}>
        <Col
          span={COL_24}
          md={COL_12}
          lg={COL_12}
          xl={COL_9}
        >
          <img  className={'img-thumbnail'} src={getImageProduct(data)} alt={data.name}/>
        </Col>
        <Col
          span={COL_24}
          md={COL_12}
          lg={COL_12}
          xl={COL_15}
        >
          <FormProperties
            data={data}
            handleSumbit={()=> {
              console.log('handleSubmit')
            }}
          />
        </Col>
      </Row>
    </div>
  ) : null}

  export default JobItem