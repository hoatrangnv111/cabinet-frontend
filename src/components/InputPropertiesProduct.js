import React, {useCallback} from 'react'
import {useDispatch} from "react-redux";
import AutoComplete from "antd/lib/auto-complete"
// extensions
import objectExtensions from '../extensions/object'
//actions
import userSessionActions from "../actions/userSessionActions"

const { Option } = AutoComplete

const DEFAULT_VALUE = 0

const InputPropertiesProduct = ({currentJobId, label, data, onChange, onBlur}) => {

  const dispatch = useDispatch()

  const changePropertyItem = useCallback(
    (item) => dispatch(userSessionActions.changePropertyItem(item)),
    [dispatch]
  )

  const getCurrentValue = (value, minValue = DEFAULT_VALUE) => {
    return value === undefined ? minValue : (value ? value : DEFAULT_VALUE)
  }

  const handleChangeInput = (key, value) => {
    if (data) {
      console.log('value === undefine', value === undefined)
      const { minValue } = getMinMaxData(data)
      const inputValue = getCurrentValue(value, minValue)
      console.log('inputValue', inputValue)
      changePropertyItem({
        currentJobId: currentJobId,
        propertyId: key,
        propertyValue: inputValue
      })
      if(onChange){
        onChange(value)
      }
    }
  }

  const getMinMaxData = (data) => {
    const minValue = data ? (data.minValue ? data.minValue : data.defaultValue || DEFAULT_VALUE) : DEFAULT_VALUE
    const maxValue = data ?  data.maxValue ? data.maxValue : minValue : DEFAULT_VALUE
    return {
      minValue: minValue,
      maxValue: maxValue
    }
  }

  const handleBlur = (data) => {
    const {minValue, maxValue} = getMinMaxData(data)
    const currentValue = getCurrentValue(data.value, minValue)
    if(currentValue && minValue && maxValue){
      if(parseFloat(currentValue) < parseFloat(minValue) || parseFloat(currentValue) > parseFloat(maxValue)){
        if(parseFloat(currentValue) < parseFloat(minValue)){
          handleChangeInput(data.id, minValue)
        }else if(parseFloat(currentValue) > parseFloat(maxValue)){
          handleChangeInput(data.id, maxValue)
        }
      }
    }
    if(onBlur) onBlur()
  }


  return (
    <AutoComplete
      className='w-100'
      value={data.value}
      defaultValue={data.defaultValue}
      allowClear
      onBlur={() => {
        handleBlur(data)
      }}
      onChange={(value) => {
        handleChangeInput(data.id, value)
      }}
    >
      {data.variables.map((item, key) => (
        <Option key={key} value={item.value}>
          {item.label}
        </Option>
      ))}
    </AutoComplete>
  )
}

export default InputPropertiesProduct