import React from 'react'
import Scroll from 'react-scroll'

const Element = Scroll.Element;
const scroller = Scroll.scroller;
const scroll = Scroll.animateScroll;

const ElementScroll = (
  {
    name, children, scrollTo, containerId,
    offset = 0, duration = 1000, delay = 100,
    scrollToTop = false, scrollToBottom = false,
    onClick
  }
) => {
  const handleClick = () => {
    if (scrollToTop || scrollToBottom) {
      if (scrollToTop) scroll.scrollToTop();
      if (scrollToBottom) scroll.scrollToBottom();
    } else {
      scroller.scrollTo(scrollTo, {
        duration: duration,
        delay: delay,
        smooth: true,
        containerId: containerId,
        offset: offset,
      })
    }
    if(onClick){
      onClick()
    }
  }
  return (
    <Element name={name} onClick={handleClick}>{children}</Element>
  )
}

export default ElementScroll