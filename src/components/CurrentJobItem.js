import React from 'react'
import Col from "antd/lib/col";
import Button from "antd/lib/button";
import {CloseCircleOutlined} from '@ant-design/icons'
// constants
import {COL_12, COL_24, COL_4, COL_8, SMALL_LABEL_LAYOUT} from "../constants/layout";
// extensions
import {getProductByCurrentJob, getPropertiesDefault} from "../extensions/product";
import {getImageProduct} from "../extensions/image";
// components
import CustomizeCard from "./CustomizeCard";
import FormProperties from "./Form/FormProperties";

const CurrentJobItem = ({data, handleChangeJobProps, handleDelete}) => {
  return data ? (
    <Col
      span={COL_24}
      md={COL_12}
      lg={COL_8}
      xl={COL_4}
    >
      <CustomizeCard
        customizeClass='mb-3 cbn-card__category'
        title={<div className='text-center'>{data?.name}</div>}
        cover={
          <div className='w-xl-100 cbn-type-image px-2 py-2 px-xl-0 py-xl-0'>
            <img src={getImageProduct(data)} className='img-fluid'/>
          </div>
        }
        extra={<a onClick={() => handleDelete(data)}><CloseCircleOutlined style={{fontSize: '20px'}}/></a>}
        actions={[
          <Button
            type='primary'
            className='w-75'
            onClick={() => handleChangeJobProps(data)}
          >
            Edit
          </Button>
        ]}
      >
        <FormProperties
          data={data}
          minimize={true}
          formLayout={SMALL_LABEL_LAYOUT}
          handleSumbit={()=> {
            console.log('handleSubmit')
          }}
        />
      </CustomizeCard>
    </Col>
  ) : null
}

export default CurrentJobItem