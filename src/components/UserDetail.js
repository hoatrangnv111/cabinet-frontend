import React, {useEffect} from "react"
import {withFormik, Field} from 'formik'
import classNames from 'classnames'
import * as Yup from 'yup'
import AntForm from "antd/lib/form";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Input from "antd/lib/input";
import {yupExtensions, formikExtensions} from '../extensions'
import {
  COL_12,
  COL_15,
  COL_24,
  COL_9,
  DEFAULT_FORM_LAYOUT,
  DEFAULT_GUTTER,
  SMALL_LABEL_LAYOUT
} from "../constants/layout";
import Button from "antd/lib/button";

const {Item} = AntForm

const ButtonBlock = ({handleSubmit}) => {
  return (
    <React.Fragment>
      <Button block className='mb-3'>
        View list of ready to use styles
      </Button>
      <Button block className='mb-3'>
        Selected Style Name
      </Button>
      <Button block type='primary' className='mb-3' onClick={handleSubmit}>
        Save Job
      </Button>
    </React.Fragment>
  )
}

const customFormik = withFormik({
  enableReinitialize: true,
  displayName: 'DataForm',
  mapPropsToValues: ({data}) => ({
    code: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'code',
      defaultFieldValue: ''
    }),
    firstName: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'firstName',
      defaultFieldValue: ''
    }),
    lastName: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'lastName'
    }),
    phoneNumber: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'phoneNumber'
    }),
    email: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'email'
    }),
    address: formikExtensions.getDefaultValueField({
      data,
      fieldName: 'address'
    }),
  }),
  handleSubmit: (values, {props}) => {
    props.handleOnSubmit(values)
  }
})


const UserDetail = (props) => {
  const {data, values, resetForm, handleSubmit} = props

  useEffect(() => {
    resetForm()
  }, [data])


  return (
    <AntForm
      {...DEFAULT_FORM_LAYOUT}
      className='w-100 cbn-user'
    >
      <Row gutter={DEFAULT_GUTTER}>
        <Col md={COL_9}>
          <ButtonBlock
            handleSubmit={handleSubmit}
          />
        </Col>
        <Col md={COL_15}>
          <Row>
            <Col span={COL_24}>
              <Item label='Job Identification' className='mb-2'>
                <span>{values?.code}</span>
              </Item>
            </Col>
          </Row>
          <Row gutter={DEFAULT_GUTTER}>
            <Col span={COL_24} lg={COL_12}>
              <Item label='First Name' className='mb-2'>
                <Field
                  name='firstName'
                  className={classNames('ant-input w-100')}
                />
              </Item>
            </Col>
            <Col span={COL_24} lg={COL_12}>
              <Item label='Last Name'  className='mb-2'>
                <Field
                  name='lastName'
                  className={classNames('ant-input w-100')}
                />
              </Item>
            </Col>
          </Row>
          <Row gutter={DEFAULT_GUTTER}>
            <Col span={COL_24} lg={COL_12}>
              <Item label='Phone' className='mb-2'>
                <Field
                  name='phoneNumber'
                  className={classNames('ant-input w-100')}
                />
              </Item>
            </Col>
            <Col span={COL_24} lg={COL_12}>
              <Item label='Email' className='mb-2'>
                <Field
                  name='email'
                  className={classNames('ant-input w-100')}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col span={COL_24}>
              <Item label='Address' {...SMALL_LABEL_LAYOUT}>
                <Field
                  name='address'
                  className={classNames('ant-input w-100')}
                />
              </Item>
            </Col>
          </Row>
        </Col>
      </Row>
    </AntForm>
  )
}

export default customFormik(UserDetail)