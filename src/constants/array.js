import { STATUS } from './enumType'

export const STATUS_ARRAY = [
  {
    key: STATUS.ACTIVE,
    label: 'Enable',
    color: 'green',
    value: true
  },
  {
    key: STATUS.DELETE,
    label: 'Disable',
    color: 'red',
    value: false
  }
]
