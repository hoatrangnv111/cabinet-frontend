export default {
  REQUIRED: 'This field is required.',
  EMAIL: 'Please enter a valid email address.',
  URL: 'Please enter a valid URL.',
  NUMBER: 'Please enter a valid number'
}
