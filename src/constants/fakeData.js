export const ITEMS_TIRE_2 = 10

export const ITEMS_TIRE_3 = 60

const ITEM = {
  name: 'Style Name',
  thumbnail:
    'https://sese.vn/uploads/shop/attachment/tu/tu-/tu-tab-go-gia-cabinet-trang-8ohcy220.jpg',
  description: 'description 1',
  kind: 'Style'
}

export const generateItems = ({defaultImage, formatName, limit, hasChildrens = true}) => {
  let outputs = []
  if (!limit) return outputs
  for (let i = 0; i < limit; i++) {
    const item = {
      ...ITEM,
      name: `${formatName ? formatName : ITEM.name} - ${i}`,
      thumbnail: defaultImage
    }
    if(hasChildrens){
      const childrens = generateItems({
        limit: ITEMS_TIRE_3,
        formatName: item.name,
        defaultImage: defaultImage,
        hasChildrens: false
      })
      item.items = childrens
    }
    outputs.push(item)
  }
  return outputs
}

export const CABINET_TYPE = [
  {
    name: "Door",
    thumbnail:
      'http://cdn3.volusion.com/ufddy.cnppv/v/vspfiles/photos/NEWYORK-IP-DOOR-2T.jpg?v-cache=1584123469',
    subDesc: 'It is a long established fact that a reader will be distracted by the readable content of ..... ',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s ' +
      'standard dummy text ever since the 1500s,',
    items: [
      {
        name: 'Door Style 1',
        thumbnail:
          'https://sese.vn/uploads/shop/attachment/tu/tu-/tu-tab-go-gia-cabinet-trang-8ohcy220.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://sese.vn/uploads/shop/attachment/tu/tu-/tu-tab-go-gia-cabinet-trang-8ohcy220.jpg` ,
          formatName: `Door Style 1`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Door Style 2',
        thumbnail:
          'https://images.homedepot-static.com/productImages/f05bdd50-24f9-4f7b-a158-73c1ffb5770b/svn/reclaimed-barnwood-welwick-designs-office-storage-cabinets-hd8241-64_1000.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://images.homedepot-static.com/productImages/f05bdd50-24f9-4f7b-a158-73c1ffb5770b/svn/reclaimed-barnwood-welwick-designs-office-storage-cabinets-hd8241-64_1000.jpg` ,
          formatName: `Door Style 2`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Door Style 3',
        thumbnail:
          'https://secure.img1-fg.wfcdn.com/im/02351441/compr-r85/1065/106548278/finley-15-bottle-wine-cabinet.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://secure.img1-fg.wfcdn.com/im/02351441/compr-r85/1065/106548278/finley-15-bottle-wine-cabinet.jpg` ,
          formatName: `Door Style 3`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Door Style 4',
        thumbnail:
          'https://secure.img1-fg.wfcdn.com/im/41315121/compr-r85/8828/88288700/whitmore-bar-cabinet-with-wine-storage.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://secure.img1-fg.wfcdn.com/im/41315121/compr-r85/8828/88288700/whitmore-bar-cabinet-with-wine-storage.jpg` ,
          formatName: `Door Style 4`,
          limit: ITEMS_TIRE_2,
        })
      },
    ]
  },
  {
    name: "Cabinet",
    thumbnail:
      'https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg',
    subDesc: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s ' +
      'standard dummy text ever since the 1500s,',
    items: [
      {
        name: 'Cabinet Style 1',
        thumbnail:
          'https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg` ,
          formatName: `Cabinet Style 1`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Cabinet Style 2',
        thumbnail:
          'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
          formatName: `Cabinet Style 2`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Cabinet Style 3',
        thumbnail:
          'https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg` ,
          formatName: `Cabinet Style 3`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Cabinet Style 4',
        thumbnail:
          'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
          formatName: `Cabinet Style 4`,
          limit: ITEMS_TIRE_2,
        })
      }
    ]
  },
  {
    name: "Drawer",
    thumbnail:
      'https://cdn2.bigcommerce.com/server5100/7c65a/products/471/images/1090/Plywood-Corner__29580.1503000608.220.220.jpg?c=2',
    subDesc: 'There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s ' +
      'standard dummy text ever since the 1500s,',
    items: [
      {
        name: 'Drawer Style 1',
        thumbnail:
          'https://cdn2.bigcommerce.com/server5100/7c65a/products/471/images/1090/Plywood-Corner__29580.1503000608.220.220.jpg?c=2',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://cdn2.bigcommerce.com/server5100/7c65a/products/471/images/1090/Plywood-Corner__29580.1503000608.220.220.jpg?c=2',
          formatName: `Drawer Style 1`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Drawer Style 2',
        thumbnail:
          'http://coronamillworks.com/wp-content/uploads/2017/02/3boxesstacked300.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'http://coronamillworks.com/wp-content/uploads/2017/02/3boxesstacked300.jpg',
          formatName: `Drawer Style 2`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Drawer Style 3',
        thumbnail:
          'https://cdn2.bigcommerce.com/server5100/7c65a/products/471/images/1090/Plywood-Corner__29580.1503000608.220.220.jpg?c=2',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://cdn2.bigcommerce.com/server5100/7c65a/products/471/images/1090/Plywood-Corner__29580.1503000608.220.220.jpg?c=2',
          formatName: `Drawer Style 3`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Drawer Style 4',
        thumbnail:
          'http://coronamillworks.com/wp-content/uploads/2017/02/3boxesstacked300.jpg',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'http://coronamillworks.com/wp-content/uploads/2017/02/3boxesstacked300.jpg',
          formatName: `Drawer Style 4`,
          limit: ITEMS_TIRE_2,
        })
      }
    ]
  },
  {
    name: "Hardware",
    thumbnail:
      'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/15_61efb136-311c-43d5-b313-645e2e5d0ffe_1024x1024@2x.jpg?v=1577941922',
    subDesc: 'It is a long established fact that a reader will be distracted by the readable content of ..... ',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s ' +
      'standard dummy text ever since the 1500s,',
    items: [
      {
        name: 'Hardware Style 1',
        thumbnail:
          'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/15_61efb136-311c-43d5-b313-645e2e5d0ffe_1024x1024@2x.jpg?v=1577941922',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://cdn.shopify.com/s/files/1/0143/6903/0230/products/15_61efb136-311c-43d5-b313-645e2e5d0ffe_1024x1024@2x.jpg?v=1577941922` ,
          formatName: `Hardware Style 1`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Hardware Style 2',
        thumbnail:
          'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/9_d5ff8002-2d1a-4ed9-9865-24280b4bde92_1024x1024@2x.jpg?v=1577941922',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: `https://cdn.shopify.com/s/files/1/0143/6903/0230/products/9_d5ff8002-2d1a-4ed9-9865-24280b4bde92_1024x1024@2x.jpg?v=1577941922` ,
          formatName: `Hardware Style 2`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Hardware Style 3',
        thumbnail:
          'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/15_61efb136-311c-43d5-b313-645e2e5d0ffe_1024x1024@2x.jpg?v=1577941922',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/15_61efb136-311c-43d5-b313-645e2e5d0ffe_1024x1024@2x.jpg?v=1577941922',
          formatName: `Hardware Style 3`,
          limit: ITEMS_TIRE_2,
        })
      },
      {
        name: 'Hardware Style 4',
        thumbnail:
          'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/9_d5ff8002-2d1a-4ed9-9865-24280b4bde92_1024x1024@2x.jpg?v=1577941922',
        description: "We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), " +
          "to help people create their product prototypes beautifully and efficiently.",
        items: generateItems({
          defaultImage: 'https://cdn.shopify.com/s/files/1/0143/6903/0230/products/9_d5ff8002-2d1a-4ed9-9865-24280b4bde92_1024x1024@2x.jpg?v=1577941922',
          formatName: `Hardware Style 4`,
          limit: ITEMS_TIRE_2,
        })
      },
    ]
  }
]


export const CATEGORIES = [
  {
    name: 'Lower',
    categories: [
      {
        name: 'Lower',
        items: [
          {
            id: 1,
            name: 'Lower A',
            thumbnail:
              'https://cdn10.bigcommerce.com/s-lo8vxi6nee/products/2557/images/4136/AZT5530__93166.1486312170.500.500.jpg?c=2'
          },
          {
            id: 2,
            name: 'Lower B',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          },
          {
            id: 3,
            name: 'Lower D',
            thumbnail:
              'https://i5.walmartimages.ca/images/Large/140/094/109066-772398140094.jpg'
          },
          {
            id: 4,
            name: 'Lower E',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          },
          {
            id: 5,
            name: 'Lower F',
            thumbnail:
              'https://i5.walmartimages.ca/images/Large/140/094/109066-772398140094.jpg'
          },
          {
            id: 6,
            name: 'Lower G',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          }
        ]
      },
      {
        name: 'Lower Corner',
        items: [
          {
            id: 7,
            name: 'Lower Corner A',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRLgOqvbMgCs5YB3L4sjqrHs0iMOeSHWruamjoz1hbIvoFXx4xN&usqp=CAU'
          },
          {
            id: 8,
            name: 'Lower Corner B',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRZkGWZK2B7YKc44i9akqusamRHmYx3Ws-9q2OyrbHd-nigChtN&usqp=CAU'
          },
          {
            id: 9,
            name: 'Lower Corner D',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR0bqD0-2EWdP0tXSWPyxkToVmxijB2pphQJF7okhY1tZ5uQ-Lq&usqp=CAU'
          },
          {
            id: 10,
            name: 'Lower Corner E',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTzI_EkLxELIOXHzrHzCUMLswAAmfpGAR2K6eIWxCsTwN0DmNgt&usqp=CAU'
          },
          {
            id: 11,
            name: 'Lower Corner F',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR0jMWM0u0jgZLrwc2gxvX7m064hB8SLopXQdTFux5-iU-xTpuf&usqp=CAU'
          },
          {
            id: 12,
            name: 'Lower Corner G',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSelMp7G8xgqfphsM3pJ4O_NT2H0C4sGCGjb9-FA8NphAy9tDof&usqp=CAU'
          }
        ]
      },
      {
        name: 'Lower appliances cabinets',
        items: [
          {
            id: 13,
            name: 'Lower A',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFUeEJlC8JNX6o4NVNS9jHoL-FP1FSiqawZHeuq5_QUR-o_W1F&usqp=CAU'
          },
          {
            id: 14,
            name: 'Lower B',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpd-YHcz8WOa4b2b6XCdx2YJqbt6jSeR1mtgm9Ol82w4-JUzXFwQ&s'
          },
          {
            id: 15,
            name: 'Lower D',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFUeEJlC8JNX6o4NVNS9jHoL-FP1FSiqawZHeuq5_QUR-o_W1F&usqp=CAU'
          },
          {
            id: 16,
            name: 'Lower E',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwONqCblNTV_OrTaUA9sX3iFrlJSsHbOTT5Py4C45ci4Mx1fNgPg&s'
          },
          {
            id: 17,
            name: 'Lower F',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwONqCblNTV_OrTaUA9sX3iFrlJSsHbOTT5Py4C45ci4Mx1fNgPg&s'
          },
          {
            id: 18,
            name: 'Lower G',
            thumbnail:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFUeEJlC8JNX6o4NVNS9jHoL-FP1FSiqawZHeuq5_QUR-o_W1F&usqp=CAU'
          }
        ]
      }
    ]
  },
  {
    name: 'Tall',
    categories: [
      {
        name: 'Tall',
        items: [
          {
            id: 19,
            name: 'Tall A',
            thumbnail:
              'https://cdn10.bigcommerce.com/s-lo8vxi6nee/products/2557/images/4136/AZT5530__93166.1486312170.500.500.jpg?c=2'
          },
          {
            id: 20,
            name: 'Tall B',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          },
          {
            id: 21,
            name: 'Tall D',
            thumbnail:
              'https://i5.walmartimages.ca/images/Large/140/094/109066-772398140094.jpg'
          },
          {
            id: 22,
            name: 'Tall E',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          },
          {
            id: 23,
            name: 'Tall F',
            thumbnail:
              'https://i5.walmartimages.ca/images/Large/140/094/109066-772398140094.jpg'
          },
          {
            id: 24,
            name: 'Tall G',
            thumbnail:
              'https://images-na.ssl-images-amazon.com/images/I/51SOEVpCQmL._AC_SX679_.jpg'
          }
        ]
      }
    ]
  },
  {name: 'Upper'},
  {name: 'Island Cabinets'},
  {name: 'Bathroom'}
]

export const CURRENT_JOB = [
  {
    name: 'Lower 1',
    thumbnail:
      'https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg',
    width: 100,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  },
  {
    name: 'Cabinet 2',
    thumbnail:
      'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
    width: 100,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  },
  {
    name: 'Door 3',
    thumbnail:
      'https://i.pinimg.com/236x/10/d0/79/10d079fb503c1ca7100f02644562e3e1.jpg',
    width: 100,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  },
  {
    name: 'Lower 4',
    thumbnail:
      'https://i.pinimg.com/236x/6f/ed/61/6fed61369ff13a67759f0dd3b2f004c0.jpg',
    width: 100,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  },
  {
    name: 'Cabinet 5',
    thumbnail:
      'https://i.pinimg.com/236x/b2/90/15/b29015abf4a231a9337db5e4c738272d.jpg',
    width: 100.0,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  },
  {
    name: 'Door 6',
    thumbnail:
      'https://i.pinimg.com/236x/10/d0/79/10d079fb503c1ca7100f02644562e3e1.jpg',
    width: 100,
    minWidth: 0,
    maxWidth: 1000,
    height: 200,
    minHeight: 0,
    maxHeight: 2000,
    depth: 200,
    minDepth: 0,
    maxDepth: 2000,
    description: 'description'
  }
]

export const PROPERTIES = [
  {
    key: 'width',
    shortDescription: 'w',
    description: 'width',
    defaultValue: 200,
    isPublished: true,
    isShowDefault: true
  },
  {
    key: 'height',
    shortDescription: 'h',
    description: 'height',
    defaultValue: 300,
    isPublished: true,
    isShowDefault: true
  },
  {
    key: 'd',
    shortDescription: 'd',
    description: 'd',
    defaultValue: 100,
    isPublished: true,
    isShowDefault: true
  },
  {
    key: 'properties-1',
    shortDescription: 'p1',
    description: 'properties 1',
    defaultValue: 100,
    isPublished: true,
    isShowDefault: false
  },
  {
    key: 'properties-2',
    shortDescription: 'p2',
    description: 'properties 2',
    defaultValue: 100,
    isPublished: false,
    isShowDefault: false
  },
]
