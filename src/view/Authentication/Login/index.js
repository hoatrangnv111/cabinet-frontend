import React, {useCallback, useEffect} from 'react'
import {useDispatch} from 'react-redux'
import queryString from 'query-string'
import Form from './Form'
import authActions from '../../../actions/authActions'
import {stringExtensions} from '../../../extensions'
import {routes} from '../../../routes/home'
import {resetFormFieldError} from '../../../actions/formActions'

const Login = (props) => {
  const dispatch = useDispatch()

  const queryClause = queryString.parse(props.location.search)
  const pathRedirect =
    queryClause && queryClause.redirectURL
      ? queryClause.redirectURL
      : routes.ROUTE_HOME_PAGE

  const loginCallback = useCallback(
    (queryClause, pathRedirect) =>
      dispatch(authActions.login(queryClause, pathRedirect)),
    [dispatch]
  )

  const resetFormCallback = useCallback(() => dispatch(resetFormFieldError()), [
    dispatch
  ])

  useEffect(() => {
    resetFormCallback()
  }, [])

  const handleSubmitForm = (values) => {
    const email = stringExtensions.removeEscapeCharacter(values.email)
    const password = stringExtensions.removeEscapeCharacter(values.password)
    const queryClause = `email: "${email}", password: "${password}"`
    loginCallback(queryClause, pathRedirect)
  }

  return (
    <div className={'form'}>
      <h4 className='text-uppercase text-center'>
        <strong>Please log in</strong>
      </h4>
      <br/>
      <Form handleSubmitForm={handleSubmitForm}/>
    </div>
  )
}

export default Login
