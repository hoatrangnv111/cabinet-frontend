import React, {useState, useCallback} from 'react'
import {useDispatch, useSelector} from "react-redux"
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Button from 'antd/lib/button'
import Modal from 'antd/lib/modal'
import { ExclamationCircleOutlined } from '@ant-design/icons';
//  actions
import userSessionActions from "../../actions/userSessionActions";
// constants
import {
  DEFAULT_GUTTER,
  HORIZONTAL_LAYOUT,
  COL_24,
  COL_16,
  COL_15,
  COL_12,
  COL_9,
  COL_8,
  COL_4
} from '../../constants/layout'
import {WIDTH_MODAL_JOB_ITEM, DEFAULT_LABEL} from "../../constants";
import {CONFIRM_MODAL} from '../../constants/message'
// extensions
import {getLabelProperties, getPropertiesDefault} from "../../extensions/product";
import {getImageProduct} from "../../extensions/image";
// components
import CustomizeCard from '../../components/CustomizeCard'
import CustomModal from '../../components/CustomModal'
import InputPropertiesProduct from '../../components/InputPropertiesProduct'
import CurrentJobItem from '../../components/CurrentJobItem'
import JobItem from "../../components/JobItem";

const CurrentJob = (props) => {

  const dispatch = useDispatch()

  const state = useSelector(state => ({
    currentJob: state.userSession ? state.userSession.currentJob || [] : []
  }))

  const [jobChange, setJobChange] = useState(null)

  const [openModal, setOpenModal] = useState(null)

  const removeCurrentJob = useCallback(
    (currentJob) => dispatch(userSessionActions.removeCurrentJob(currentJob)),
    [dispatch]
  )

  const handleChangeJobProps = (data) => {
    setOpenModal(true)
    setJobChange(data)
  }

  const handleDelete = (data) => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: CONFIRM_MODAL,
      okText: 'OK',
      cancelText: 'Cancel',
      onOk: () => removeCurrentJob(data)
    });
  }

  return (
    <CustomizeCard
      title='Current Job'
      customizeClass={'session-jobs'}
      id={'job-list'}
    >
      <div className='my-3'>
        <Row gutter={DEFAULT_GUTTER}>
          {state.currentJob?.length > 0
            ? state.currentJob.map((job, index) => (
              <CurrentJobItem
                key={index}
                data={job}
                handleChangeJobProps={handleChangeJobProps}
                handleDelete={handleDelete}
              />
            ))
             : null}
        </Row>
      </div>
      <CustomModal
        width={WIDTH_MODAL_JOB_ITEM}
        isOpen={openModal}
        title={`Edit ${jobChange?.name}`}
        handleAccept={() => {
          setOpenModal(false)
        }}
        handleCancel={() => {
          setOpenModal(false)
        }}
      >
        <JobItem data={jobChange}/>
      </CustomModal>
    </CustomizeCard>
  )
}

export default CurrentJob
