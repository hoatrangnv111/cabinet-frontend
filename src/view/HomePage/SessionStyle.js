import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
//HOCS
import { withLoadData } from '../../hocs/withLoadData'
// actions
import userSessionActions from '../../actions/userSessionActions'
// clause search
import featureCategoryClause from '../../clauseSearch/featureCategory'
// reselect
import { selectActiveUserSessionStyle } from '../../reselect/useSession'
// constants
import { FEATURE_CATEGORY_PATH } from '../../constants/path'
//extensions
// components
import BlockStyle from '../../components/Style/BlockStyle'
import { selectFeatureCategories } from '../../reselect/featureCategory'

const SessionStyle = (props) => {
  const dispatch = useDispatch()

  const [firstLoad, setFirstLoad] = useState(false)

  const setActiveStyleCallback = useCallback(
    (isCollage, data) =>
      dispatch(userSessionActions.setActiveStyle(isCollage, data)),
    [dispatch]
  )

  const loadSessionActiveCallback = useCallback(
    () => dispatch(userSessionActions.loadSessionActive()),
    [dispatch]
  )

  const featureCategories = useSelector(selectFeatureCategories())

  const { activeStyle } = useSelector(selectActiveUserSessionStyle())

  useEffect(() => {
    if (!firstLoad) {
      if (featureCategories?.length > 0) {
        if (!activeStyle) {
          handleToggleCollage(featureCategories[0].defaultStyle)
        } else {
          loadSessionActiveCallback()
        }
        setFirstLoad(true)
      }
    }
  }, [featureCategories])

  const handleToggleCollage = (active) => {
    const isCollapse = !!(activeStyle && active.id === activeStyle)
    console.log('isCollapse......', isCollapse)
    setActiveStyleCallback(isCollapse, active)
  }

  return featureCategories.map((item, key) => {
    return (
      <BlockStyle
        key={key}
        data={item.featureCategory}
        defaultStyle={item.defaultStyle}
        handleToggleCollage={handleToggleCollage}
      />
    )
  })
}

export default withLoadData({
  pathName: FEATURE_CATEGORY_PATH,
  loadData: ({ loadDataPagerCallback }) => {
    const queryClause = featureCategoryClause.initQueryFeatureCategories()
    loadDataPagerCallback(queryClause)
  }
})(SessionStyle)
