import React , {useCallback}from 'react'
import UserDetail from '../../components/UserDetail'

const UserInfo = ({userInfor, handleSaveJob}) => {
  return (
    <UserDetail
      data={userInfor}
      handleOnSubmit={(values) => {handleSaveJob(values)}}
    />
  )
}

export default UserInfo
