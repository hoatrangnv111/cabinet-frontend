import React, {useEffect, useRef, useCallback, Fragment} from 'react'
import {useDispatch, useSelector} from "react-redux";
import classNames from 'classnames'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Button from 'antd/lib/button'
import Modal from "antd/lib/modal";
import {ExclamationCircleOutlined} from '@ant-design/icons';
import {StickyContainer, Sticky} from 'react-sticky';
// actions
import userSessionActions from "../../actions/userSessionActions";
// reselect
import {selectCurrentSession, selectUserInfor} from "../../reselect/useSession";
// HOCS
import {withLoadData} from "../../hocs/withLoadData";
//HOOKs
import {usePrevious} from "../../hooks/usePrevious";
// clause
import userSessionClause from "../../clauseSearch/userSession";
// constants
import {USER_SESSION_PATH} from "../../constants/path";
import {DEFAULT_GUTTER, COL_24, COL_8, COL_16} from '../../constants/layout'
// components
import UserInfo from './UserInfo'
import SessionStyple from './SessionStyle'
import SessionFeature from './SessionFeature'
import Category from './Category'
import CurrentJob from './CurrentJob'
import ModalConfirm from '../../components/Modal/ModalConfirm'


import {CONFIRM_MODAL} from "../../constants/message";
import Sidebar from "../../components/Layout/Sidebar";

const HomePage = ({objectId}) => {

  const dispatch = useDispatch()

  const userInfor = useSelector(selectUserInfor())

  const currentSession = useSelector(selectCurrentSession())

  const loadSession = useCallback(
    (objectId) => dispatch(userSessionActions.loadData(objectId, USER_SESSION_PATH)),
    [dispatch]
  )

  const saveJob = useCallback(
    (currentSession, userInfor) => dispatch(userSessionActions.saveJob(currentSession, userInfor)),
    [dispatch]
  )

  const handleSaveJob = (userInfor) => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined/>,
      content: CONFIRM_MODAL,
      okText: 'OK',
      cancelText: 'Cancel',
      onOk: () => {
        //saveJob(currentSession, userInfor)
        console.log('save job success')
      }
    });
  }

  const previousParams = usePrevious(objectId)

  useEffect(() => {
    //console.log('loading', objectId)
    loadSession(objectId)
  }, [objectId])

  return (
    <StickyContainer className='container content-page my-3 position-relative'>
      <Sticky topOffset={20}>{({style, isSticky, wasSticky}) => {
        return(
          <div style={style}>
            <Sidebar
              className={classNames({
                'sticky': isSticky && wasSticky
              })}
              handleSaveJob={handleSaveJob}
            />
          </div>
        )
      }}</Sticky>
      <div>
        <UserInfo
          userInfor={userInfor}
          handleSaveJob={handleSaveJob}
        />
        <SessionStyple/>
        <hr className='my-3'/>
        <Category/>
        <hr className='mt-0 mb-3'/>
        <CurrentJob/>
        <hr className='my-3'/>
        <Button type='primary'>Create CSV</Button>
        <ModalConfirm/>
      </div>
    </StickyContainer>

  )
}

export default withLoadData({
  pathName: USER_SESSION_PATH,
  loadData: ({currentUser, loadDataPagerCallback}) => {
    if (currentUser) {
      const queryClause = userSessionClause.initQueryUserSessions({
        user: currentUser.id
      })
      loadDataPagerCallback(queryClause)
    }
  }
})(HomePage)

