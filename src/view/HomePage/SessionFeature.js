import React, {useCallback, useEffect, useRef, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import Col from "antd/lib/col";
import Row from "antd/lib/row";
// HoCs
import {withLoadData} from "../../hocs/withLoadData";
// clause
import featureClause from "../../clauseSearch/feature";
import featureProductClause from "../../clauseSearch/featureProduct";
// actions
import dataActions from "../../actions/dataActions";
// selector
import {selectFeatures} from "../../reselect/feature";
import {selectFeatureProducts} from "../../reselect/featureProduct";
// constants
import {FEATURE_PATH, FEATURE_PRODUCT_PATH} from "../../constants/path";
import {COL_16, COL_24, COL_8, DEFAULT_GUTTER} from "../../constants/layout";
// components
import ListType from "./ListType";
import CurrentType from "./CurrentType";

const SessionFeature = (props) => {

  const {isLoadingPage} = props

  const dispatch = useDispatch()

  const [featureActive, setFeatureActive] = useState(null)

  const features = useSelector(selectFeatures())

  const featureProducts = useSelector(selectFeatureProducts())

  const loadFeatureProducts = useCallback(
    (clause, path) => dispatch(dataActions.loadDataPager(clause, path)),
    [dispatch]
  )

  useEffect(() => {
    if (!featureActive && features.length > 0) {
      handleChangeActiveType(features[0])
    }
  }, [features])

  const handleChangeActiveType = (feature) => {
    if (feature) {
      setFeatureActive(feature)
      handleChangeStyle()
      if (features.length > 0) {
        const queryClause = featureProductClause.initQueryGetAllFeatureProduct({
          feature: feature?.id
        })
        loadFeatureProducts(queryClause, FEATURE_PRODUCT_PATH)
      }
    }
  }

  const currentTypeRef = useRef(null)

  const handleChangeStyle = () => {
    if (currentTypeRef?.current) {
      currentTypeRef.current.scrollIntoView({behavior: 'smooth'})
    }
  }

  return (
    <Row gutter={DEFAULT_GUTTER}>
      <Col span={COL_24} xl={COL_8}>
        <ListType
          data={features}
          isLoadingPage={isLoadingPage}
          featureActive={featureActive}
          handleChangeActiveType={handleChangeActiveType}
        />
      </Col>
      <Col span={COL_24} xl={COL_16}>
        <div ref={currentTypeRef} className={'block-products'}>
          {
            featureActive ? (
              <CurrentType
                data={{
                  ...featureActive,
                  items:featureProducts
                }}
              />
            ) : null
          }
        </div>
      </Col>
    </Row>
  )
}

export default withLoadData({
  pathName: FEATURE_PATH,
  loadData: ({loadDataPagerCallback}) => {
    const queryClause = featureClause.initQueryFeatures()
    loadDataPagerCallback(queryClause)
  }
})(SessionFeature)