import React, {useCallback, useState, useEffect} from 'react'
import {useDispatch,useSelector} from "react-redux"
import PropTypes from 'prop-types'
import Tabs from 'antd/lib/tabs'
import Empty from 'antd/lib/empty'
import { v4 as uuidv4 } from 'uuid';
//HoCs
import {withLoadData} from '../../hocs/withLoadData'
// actions
import userSessionActions from "../../actions/userSessionActions"
import dataActions from "../../actions/dataActions"
// clause search
import categoryClause from '../../clauseSearch/category'
import productClause from '../../clauseSearch/product'
// reselect
import { selectCategories } from '../../reselect/category'
import { selectProductByCategory, selectLoadingProduct } from '../../reselect/product'
// constants
import {CATEGORIES, PROPERTIES} from '../../constants/fakeData'
import {
  DEFAULT_GUTTER
} from '../../constants/layout'
import {CATEGORY_PATH, PRODUCT_PATH} from '../../constants/path'
//extenstion
import {getImageProduct} from '../../extensions/image'
// components
import CustomizeCard from '../../components/CustomizeCard'
import CustomModal from '../../components/CustomModal'
import CustomSpin from "../../components/CustomSpin";
import CategoryItem from "../../components/CategoryItem";

const {TabPane} = Tabs

const Category = (props) => {

  const dispatch = useDispatch()

  const categories = useSelector(selectCategories())

  const productByCategory = useSelector(selectProductByCategory())

  const isLoadingProduct = useSelector(selectLoadingProduct())

  const [activeTab, setActiveTab] = useState(null)

  const addCurrentJob = useCallback(
    (product) => dispatch(userSessionActions.addCurrentJob(product)),
    [dispatch]
  )

  const loadProducts = useCallback(
    (clause, path) => dispatch(dataActions.loadDataPager(clause, path)),
    [dispatch]
  )

  useEffect(() => {
    if(!activeTab && categories.length > 0){
      handleChangeTabs(categories[0]?.id)
    }
  }, [categories])

  const handleClickProduct = (product) => {
    // add current job to redux
    addCurrentJob(product)
  }

  const handleChangeTabs = (categoryId) => {
    setActiveTab(categoryId)
    const queryClause = productClause.initQuerySearchProduct({
      category: categoryId
    })
    loadProducts(queryClause, PRODUCT_PATH)
  }

  return (
    <CustomSpin
      spinning={props.isLoadingPage}
    >
      {
        categories.length > 0 ? (
          <Tabs
            id={'cat-list'}
            type='card'
            className='cbn-tab__category'
            onChange={handleChangeTabs}
          >
            {categories.map((item, index) => (
              <TabPane tab={item.name} key={item.id}>
                <div className='my-3'>
                  <CustomSpin
                    spinning={isLoadingProduct}
                  >
                    {productByCategory.length > 0
                      ? productByCategory.map((category, indexCat) => (
                        <CategoryItem
                          data={category}
                          handleClickCategory={handleClickProduct}
                          key={indexCat}
                        />
                      ))
                      : <Empty />
                    }
                  </CustomSpin>
                </div>
              </TabPane>
            ))}
          </Tabs>
        ) : null
      }
    </CustomSpin>
  )
}

Category.propTypes = {
  categories: PropTypes.array
}
export default withLoadData({
  pathName: CATEGORY_PATH,
  loadData: ({loadDataPagerCallback}) => {
    const queryClause = categoryClause.initQueryRootCategories()
    loadDataPagerCallback(queryClause)
  }
})(Category)
