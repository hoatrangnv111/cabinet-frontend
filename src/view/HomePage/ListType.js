import React, {useCallback, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Card from 'antd/lib/card'
import Empty from 'antd/lib/empty'
// constants
import {UPLOAD_FEATURE} from "../../constants";
import {DEFAULT_GUTTER, COL_24, COL_12, COL_8} from '../../constants/layout'
// extensions
import {generateImages} from "../../extensions/image";
import {getFeatureTitle} from "../../extensions/feature";
// components
import CustomizeCard from '../../components/CustomizeCard'
import CustomSpin from "../../components/CustomSpin";


const ListType = (props) => {
  const {data, featureActive, isLoadingPage, handleChangeActiveType} = props

  return (
    <CustomSpin
      spinning={isLoadingPage}
    >
      {
        data?.length > 0 ? (
          <Row gutter={DEFAULT_GUTTER}>
            {data.map((feature, index) => (
              <React.Fragment key={Math.random()}>
                <Col span={COL_12} md={COL_8} xl={COL_12}>
                  <div
                    onClick={() => handleChangeActiveType(feature)}
                    className={classNames('mb-3 cursor-pointer', {
                      'border-primary border rounded': featureActive?.id === feature.id
                    })}
                  >
                    <CustomizeCard
                      title={<div className='text-center'>{getFeatureTitle(feature)}</div>}
                      cover={
                        <div className='px-2 py-2 cbn-type-image'>
                          <img
                            src={generateImages({
                              object: feature,
                              dir: UPLOAD_FEATURE
                            })}
                            className='img-fluid'
                          />
                        </div>
                      }
                    >
                      <Card.Meta title={feature?.name} className='text-center'/>
                    </CustomizeCard>
                  </div>
                </Col>
                {index % 2 !== 0 ? (
                  <Col span={COL_24} lg={COL_24} className='d-none d-xl-block'>
                    <hr className='mt-0 mb-3'/>
                  </Col>
                ) : null}
              </React.Fragment>
            ))}
          </Row>
        ) : null
      }
    </CustomSpin>
  )
}

ListType.propTypes = {
  handleChangeStyle: PropTypes.func
}

export default ListType
