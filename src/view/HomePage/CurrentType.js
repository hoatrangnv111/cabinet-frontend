import React, {useEffect} from 'react'
import {useSelector} from "react-redux";
import {withFormik, FieldArray, Field} from 'formik'
import PropTypes from 'prop-types'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import AntForm from 'antd/lib/form'
// constants
import {
  DEFAULT_GUTTER,
  COL_24,
  COL_6,
  COL_18,
  HORIZONTAL_LAYOUT,
  COL_8,
  COL_16
} from '../../constants/layout'
import {UPLOAD_FEATURE} from "../../constants";
// extensions
import {generateImages} from "../../extensions/image";
// components
import CustomizeCard from '../../components/CustomizeCard'
import {selectLoadingFeatureProduct} from "../../reselect/featureProduct";
import CustomSpin from "../../components/CustomSpin";
import CurrentTypeItem from "../../components/CurrentTypeItem";

const {Item} = AntForm

const customFormilk = withFormik({
  enableReinitialize: true,
  mapPropsToValues: ({data}) => ({
    data: data?.items ?? []
  })
})

const CurrentType = (props) => {
  const {data, values, resetForm} = props

  const isLoading = useSelector(selectLoadingFeatureProduct())

  useEffect(() => {
    resetForm()
  }, [data])

  return (
    <CustomSpin
      spinning={isLoading}
    >
      <AntForm {...HORIZONTAL_LAYOUT}>
        <CustomizeCard
          title={<div className='font-weight-bold'>{data?.name}</div>}
        >
          <FieldArray
            name='data'
            render={() =>
              values.data.map((item, index) => (
                <CurrentTypeItem
                  key={index}
                  title={`${item.feature?.type} - ${item.name}`}
                  image={
                    generateImages({
                      object: item,
                      dir: UPLOAD_FEATURE
                    })
                  }
                  description={item.description}
                />
              ))
            }
          />
        </CustomizeCard>
      </AntForm>
    </CustomSpin>
  )
}

CurrentType.propTypes = {
  data: PropTypes.object
}

export default customFormilk(CurrentType)
