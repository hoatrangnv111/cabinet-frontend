import LoadableComponent from '../components/LoadableComponent'

export const routes = {
  ROUTE_HOME_PAGE: '/',
  ROUTE_USER_SESSION: '/session',
  ROUTER_LOGIN: `/auth/login`
}

export const resources = {
  LOGIN: {
    title: 'Login',
    key: 'LOGIN'
  }
}

export const navHome = [
  {
    title: 'User Jobs',
    path: `${routes.ROUTE_USER_SESSION}/:id`,
    component: LoadableComponent(() => import('../view/HomePage')),
    isProtected: true
  },
  {
    title: 'Home',
    path: `${routes.ROUTE_HOME_PAGE}`,
    component: LoadableComponent(() => import('../view/HomePage')),
    isProtected: true
  },
]


export const navAuth = [
  {
    name: resources.LOGIN.title,
    key: resources.LOGIN.key,
    resource: resources.LOGIN.key,
    path: routes.ROUTER_LOGIN,
    component: LoadableComponent(() => import('../view/Authentication/Login')),
    isMenu: false,
    isProtected: false
  }
]